--insert data users
INSERT INTO User (account, password, registerCode) VALUES ('dbatista', '123456', '0000403201');

--insert data student
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('0000376522','Shigeo','Tsukazan','Shiroma','26/01/1993','CI','6345872',77055568,3376153,'101','376522','shigeots','12345678','stsukazan.est.@utepsa.edu','');
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('000046885A', 'Geraldo', 'Figueroa','Zurita','19/02/1997','CI','9746660',78590523,3536889,'101','46885A','geraldofz','12345678','gfigueroa.est@utepsa.edu', '');
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('0000403201', 'David', 'Batista','Taboada','19/02/1997','CI','9746660',78590523,3536889,'101','0000403201','dbatista','12345678','gfigueroa.est@utepsa.edu', '');

--insert data notification
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Primera noticia', 'Esta es la primera noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Segunda noticia', 'Esta es la segunda noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Tercera noticia', 'Esta es la tercera noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Cuarta noticia', 'Esta es la Cuarta noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Quinta noticia', 'Esta es la Quinta noticia del sistema.', 'NOTICE');

--insert data audience
INSERT INTO Audience (notification_id, target, type) VALUES (1,'PUBLIC', 'PUBLIC');
INSERT INTO Audience (notification_id, target, type) VALUES (2,'PUBLIC', 'PUBLIC');
INSERT INTO Audience (notification_id, target, type) VALUES (3,'374201', 'STUDENT');
INSERT INTO Audience (notification_id, target, type) VALUES (4,'376522', 'STUDENT');
INSERT INTO Audience (notification_id, target, type) VALUES (5,'374201', 'STUDENT');

--insert data historyNotes
INSERT INTO HistoryNotes (registerCode, careerDescription, agendaDescription, courseCode, courseDescription, note, minimunNote) VALUES ('0000403201', 'Ingenieria de Sistemas', '0000403201', '579', 'ORGANIZACION PERSONAL (FET)', 98, 51);
INSERT INTO HistoryNotes (registerCode, careerDescription, agendaDescription, courseCode, courseDescription, note, minimunNote) VALUES ('0000403201', 'Ingenieria de Sistemas', '0000403201', '580', 'ELEMENTOS DE ARITMETICA', 85, 51);



--insert data courses
--INSERT INTO Course (code, initials, name, careerCode) VALUES(579,'COM-100','ORGANIZACION PERSONAL (FET)','123-ABC');
--INSERT INTO Course (code, initials, name, careerCode) VALUES(580,'EXT-100','ELEMENTOS DE ARITMETICA','123-ABC');
--INSERT INTO Course (code, initials, name, careerCode) VALUES(581,'COM-110','INGLES I','123-ABC');
--INSERT INTO Course (code, initials, name, careerCode) VALUES(582,'SIS-100','DESARROLLO DE APLICACIONES PARA INTERNET I','123-ABC');
--INSERT INTO Course (code, initials, name, careerCode) VALUES(1089,'SIP-100','INTRODUCCION A LA INFORMATICA','123-ABC');
--INSERT INTO Course (code, initials, name, careerCode) VALUES(584,'SIS-110','INTRODUCCION A LA PROGRAMACION','123-ABC');

--insert data documentsStudent
INSERT INTO DocumentsStudent (registerCode, documentCode, dateReceipt, dateDelivery, typeDocumentPaper, description, state) VALUES('0000403201', '001', '01/11/16', '01/11/16', 'copia', 'Fotocopia de carnet de identidad', 'FALTA');
INSERT INTO DocumentsStudent (registerCode, documentCode, dateReceipt, dateDelivery, typeDocumentPaper, description, state) VALUES('0000403201', '002', '01/11/16', '01/11/16', 'copia', 'Fotocopia de certificado de nacimiento', 'FALTA');
INSERT INTO DocumentsStudent (registerCode, documentCode, dateReceipt, dateDelivery, typeDocumentPaper, description, state) VALUES('0000403201', '003', '01/11/16', '01/11/16', 'original', 'Certificado de bachiller', 'ENTREGADO');