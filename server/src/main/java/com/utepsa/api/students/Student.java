package com.utepsa.api.students;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Table;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * Created by roberto on 12/7/2016.
 */
@Entity
@Table(appliesTo = "Student")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.api.students.Student.findAll",
                query = "SELECT s FROM Student s"
        ),
        @NamedQuery(
                name = "com.utepsa.api.students.Student.findByDocumentCode",
                query = "SELECT s FROM Student s WHERE s.documentCode = :documentCode"
        ),
        @NamedQuery(
                name = "com.utepsa.api.students.Student.findByDocumentCodeOrRegisterCode",
                query = "SELECT s FROM Student s WHERE s.registerCode = :documentCodeOrRegisterCode OR s.documentCode = :documentCodeOrRegisterCode"
        ),
        @NamedQuery(
                name = "com.utepsa.api.students.Student.findByUserAccountOrRegisterCode",
                query = "SELECT s FROM Student s WHERE s.registerCode = :userAccountOrRegisterCode OR s.userAccount = :userAccountOrRegisterCode"
        ),
        @NamedQuery(
                name = "com.utepsa.api.students.Student.getStudentsGcmId",
                query = "SELECT s.gcmId FROM Student s WHERE  s.gcmId <> '' AND s.gcmId <> null"
        )
})
@ApiModel(value = "Student entity", description = "Complete info of a entity Student")
public class Student {

    public static final int CI_MIN_LENGTH = 7;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "The id of the Student")
    private long id;

    @Column(name = "agendaCode", nullable = false)
    @ApiModelProperty(value = "The agenda code of the Student", required = true)
    private String agendaCode;

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Student", required = true)
    private String name;

    @Column(name = "fatherLastName", nullable = false)
    @ApiModelProperty(value = "The father's last name of the Student", required = true)
    private String fatherLastName;

    @Column(name = "motherLastName", nullable = false)
    @ApiModelProperty(value = "The mother's last name of the Student", required = true)
    private String motherLastName;

    @Column(name = "birthdate", nullable = true)
    @ApiModelProperty(value = "The birthdate of the Student", required = true)
    private String birthdate;

    @Column(name = "documentType", nullable = false)
    @ApiModelProperty(value = "The document that use the student to register", required = true)
    private String documentType;

    @Column(name = "documentCode", nullable = false)
    @ApiModelProperty(value = "The code of the document use the student to register", required = true)
    private String documentCode;

    @Column(name = "phoneNumber1", nullable = true)
    @ApiModelProperty(value = "The phone number of the Student", required = false)
    private String phoneNumber1;

    @Column(name = "phoneNumber2", nullable = true)
    @ApiModelProperty(value = "The phone number of the Student", required = false)
    private String phoneNumber2;

    @Column(name = "careerCode", nullable = false)
    @ApiModelProperty(value = "The career code of the Student", required = false)
    private String careerCode;

    @Column(name = "registerCode", nullable = false)
    @ApiModelProperty(value = "The code register of the Student", required = false)
    private String registerCode;

    @Column(name = "userAccount", nullable = false)
    @ApiModelProperty(value = "The user account of the Student", required = false)
    private String userAccount;

    @Column(name = "password", nullable = false)
    @ApiModelProperty(value = "The password  account of the Student", required = false)
    private String password;

    @Column(name = "email", nullable = false)
    @ApiModelProperty(value = "The email of the Student", required = false)
    private String email;

    @Column(name = "gcmId", nullable = true)
    @ApiModelProperty(value = "The gcm_id of the Students device", required = false)
    private String gcmId;

    public Student() {}

    public Student(long id, String agendaCode, String name, String fatherLastName, String motherLastName, String birthdate, String documentType, String documentCode, String phoneNumber1, String phoneNumber2, String careerCode, String registerCode, String userAccount, String password, String email, String gcmId) {
        this.id = id;
        this.agendaCode = agendaCode;
        this.name = name;
        this.fatherLastName = fatherLastName;
        this.motherLastName = motherLastName;
        this.birthdate = birthdate;
        this.documentType = documentType;
        this.documentCode = documentCode;
        this.phoneNumber1 = phoneNumber1;
        this.phoneNumber2 = phoneNumber2;
        this.careerCode = careerCode;
        this.registerCode = registerCode;
        this.userAccount = userAccount;
        this.password = password;
        this.email = email;
        this.gcmId = gcmId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAgendaCode() {
        return agendaCode;
    }

    public void setAgendaCode(String agendaCode) {
        this.agendaCode = agendaCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherLastName() {
        return fatherLastName;
    }

    public void setFatherLastName(String fatherLastName) {
        this.fatherLastName = fatherLastName;
    }

    public String getMotherLastName() {
        return motherLastName;
    }

    public void setMotherLastName(String motherLastName) {
        this.motherLastName = motherLastName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getCareerCode() {
        return careerCode;
    }

    public void setCareerCode(String careerCode) {
        this.careerCode = careerCode;
    }

    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return  Objects.equals(getId(), student.getId()) &&
                Objects.equals(getAgendaCode(), student.getAgendaCode()) &&
                Objects.equals(getName(), student.getName()) &&
                Objects.equals(getFatherLastName(), student.getFatherLastName()) &&
                Objects.equals(getMotherLastName(), student.getMotherLastName()) &&
                Objects.equals(getBirthdate(), student.getBirthdate()) &&
                Objects.equals(getDocumentType(), student.getDocumentType()) &&
                Objects.equals(getDocumentCode(), student.getDocumentCode()) &&
                Objects.equals(getPhoneNumber1(), student.getPhoneNumber1()) &&
                Objects.equals(getPhoneNumber2(), student.getPhoneNumber2()) &&
                Objects.equals(getCareerCode(), student.getCareerCode()) &&
                Objects.equals(getRegisterCode(), student.getRegisterCode()) &&
                Objects.equals(getUserAccount(), student.getUserAccount()) &&
                Objects.equals(getPassword(), student.getPassword()) &&
                Objects.equals(getEmail(), student.getEmail()) &&
                Objects.equals(getGcmId(), student.getGcmId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAgendaCode(), getName(), getFatherLastName(), getMotherLastName(), getBirthdate(), getDocumentType(), getDocumentCode(), getPhoneNumber1(), getPhoneNumber2(), getCareerCode(), getRegisterCode(), getUserAccount(), getPassword(), getEmail(), getGcmId());
    }

    @Override
    public String toString() {
        return String.format("Student {id=%d,agendaCode=%s,name=%s,fatherLastName=%s,motherLastName=%s,birthdate=%s,documentType=%s,documentCode=%s,phoneNumber1=%s,phoneNumber2=%s,careerCode=%s,registerCode=%s,userAccount=%s,password=%s,email=%s, gcmId=%s}",
                this.id, this.agendaCode, this.name, this.fatherLastName, this.motherLastName, this.birthdate, this.documentType, this.documentCode, this.phoneNumber1, this.phoneNumber2, this.careerCode, this.registerCode, this.userAccount, this.password, this.email, this.gcmId);
    }
}
