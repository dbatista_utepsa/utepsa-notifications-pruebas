package com.utepsa.api.courses;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Table;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Created by JoseCarlos on 27-07-16.
 */

@Entity
@Table(appliesTo = "Course")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.api.courses.Course.findAll",
                query = "SELECT c FROM Course c"
        )
})

@ApiModel(value = "Course entity", description = "Complete info of a entity Course")
public class Course {
    public static final int CODE_MIN_LENGTH = 1;

    @Id
    @Column(name = "code", nullable = false)
    @ApiModelProperty(value = "The code of the Course", required = true)
    private long code;

    @NotEmpty
    @Column(name = "initials", nullable = false)
    @ApiModelProperty(value = "The initials of the Course", required = true)
    private String initials;

    @NotEmpty
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the Course", required = true)
    private String name;

    @NotEmpty
    @Column(name = "careerCode", nullable = false)
    @ApiModelProperty(value = "The careerCode of the Course", required = true)
    private String careerCode;

    public Course() {}

    public Course(long code, String initials, String name, String careerCode) {
        this.code = code;
        this.initials = initials;
        this.name = name;
        this.careerCode = careerCode;
    }

    @JsonProperty
    public long getCode() {
        return code;
    }

    @JsonProperty
    public void setCode(long code) {
        this.code = code;
    }

    @JsonProperty
    public String getInitials() {
        return initials;
    }

    @JsonProperty
    public void setInitials(String initials) {
        this.initials = initials;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getCareerCode() {
        return careerCode;
    }

    @JsonProperty
    public void setCareerCode(String careerCode) {
        this.careerCode = careerCode;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Course)) return false;
        Course course = (Course) o;
        return  Objects.equals(getCode(), course.getCode()) &&
                Objects.equals(getInitials(), course.getInitials()) &&
                Objects.equals(getName(), course.getName()) &&
                Objects.equals(getCareerCode(), course.getCareerCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode(), getInitials(), getName(), getCareerCode());
    }

    @Override
    public String toString() {
        return String.format("Course {code=%d,initials=%s,name=%s,careerCode=%s}",
                this.code, this.initials, this.name, this.careerCode);
    }
}
