package com.utepsa.api.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Table;

/**
 * Created by david on 5/8/16.
 */

@Entity
@ApiModel(value = "User entity", description = "Complete info of a entity User")
@Table(appliesTo = "User")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.api.auth.User.loginByAccountOrRegisterCode",
                query = "SELECT u FROM User u WHERE (u.account = :accountOrRegisterCode OR u.registerCode = :accountOrRegisterCode) AND u.password = :password"
        ),
        @NamedQuery(
                name = "com.utepsa.api.auth.User.findByAccountOrRegisterCode",
                query = "SELECT u FROM User u WHERE u.account = :accountOrRegisterCode OR u.registerCode = :accountOrRegisterCode"
        )
})

public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "The id of the User", required = true)
    @Column(name = "id", nullable = false)
    private long id;

    @NotNull
    @Column(name = "account", nullable = false)
    private String account;

    @NotNull
    @Column(name = "registerCode", nullable = false)
    @ApiModelProperty(value = "The registerCode of the Student", required = true)
    private String registerCode;

    @NotEmpty
    @Column(name = "password", nullable = false)
    @ApiModelProperty(value = "The password of the User", required = true)
    private String password;

    public User(){}

    public User(long id, String account, String registerCode, String password) {
        this.id = id;
        this.account = account;
        this.registerCode = registerCode;
        this.password = password;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getAccount() { return account; }

    public void setAccount(String account) { this.account = account; }

    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (account != null ? !account.equals(user.account) : user.account != null) return false;
        if (registerCode != null ? !registerCode.equals(user.registerCode) : user.registerCode != null) return false;
        return password != null ? password.equals(user.password) : user.password == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (registerCode != null ? registerCode.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("User{id=%d, account=%s, registerCode=%s, password=%s}", this.id, this.account ,this.registerCode, this.password);
    }
}
