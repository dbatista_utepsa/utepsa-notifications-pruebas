package com.utepsa.api.historyNotes;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by Alexis Ardaya on  10/12/2016.
 */

@Entity
@Table(appliesTo = "HistoryNotes")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.api.historyNotes.historyNotes.historyNotesByStudent",
                query = "SELECT h FROM HistoryNotes h WHERE h.registerCode = :registerCode"
        ),
        @NamedQuery(
                name = "com.utepsa.api.historyNotes.historyNotes.historyNotesByStudentAndCourse",
                query = "SELECT h FROM HistoryNotes h WHERE h.registerCode = :studentRegisterCode AND h.courseCode = :courseCode"
        )
})

@ApiModel(value = "HistoryNotes entity", description = "Complete info of a entity HistoryNotes")
public class HistoryNotes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "The id of the HistoryNotes")
    private long id;

    @Column(name = "registerCode", nullable = true)
    @ApiModelProperty(value = "The register code of the Student", required = true)
    private String registerCode;

    @Column(name = "courseCode", nullable = true)
    @ApiModelProperty(value = "The code of a Course", required = true)
    private String courseCode;

    @Column(name = "courseDescription", nullable = true)
    @ApiModelProperty(value = "The Description of a Course", required = true)
    private String courseDescription;

    @Column(name = "note", nullable = true)
    @ApiModelProperty(value = "The note of a Course", required = true)
    private long note;

    @Column(name = "minimunNote", nullable = true)
    @ApiModelProperty(value = "The minimun Note of a Course", required = true)
    private long minimunNote;

    public HistoryNotes() {
    }

    public HistoryNotes(long id, String registerCode, String courseCode, String courseDescription, long note, long minimunNote) {
        this.id = id;
        this.registerCode = registerCode;
        this.courseCode = courseCode;
        this.courseDescription = courseDescription;
        this.note = note;
        this.minimunNote = minimunNote;
    }
    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getRegisterCode() {
        return registerCode;
    }

    @JsonProperty
    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    @JsonProperty
    public String getCourseCode() {
        return courseCode;
    }

    @JsonProperty
    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    @JsonProperty
    public String getCourseDescription() {
        return courseDescription;
    }

    @JsonProperty
    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    @JsonProperty
    public long getNote() {
        return note;
    }

    @JsonProperty
    public void setNote(long note) {
        this.note = note;
    }

    @JsonProperty
    public long getMinimunNote() {
        return minimunNote;
    }

    @JsonProperty
    public void setMinimunNote(long minimunNote) {
        this.minimunNote = minimunNote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HistoryNotes)) return false;
        HistoryNotes historyNotes = (HistoryNotes) o;
        return  Objects.equals(getRegisterCode(), historyNotes.getRegisterCode()) &&
                Objects.equals(getCourseCode(), historyNotes.getCourseCode()) &&
                Objects.equals(getCourseDescription(), historyNotes.getCourseDescription()) &&
                Objects.equals(getNote(), historyNotes.getNote()) &&
                Objects.equals(getMinimunNote(), historyNotes.getMinimunNote());
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(getRegisterCode(), getCourseCode(), getCourseDescription(), getNote(), getMinimunNote());
    }
    
    @Override
    public String toString() {
        return String.format("HistoryNotes {id=%d,registerCode=%s,courseCode=%s,courseDescription=%s,note=%s,minimunNote=%s}",
                this.id, this.registerCode, this.courseCode, this.courseDescription, this.note, this.minimunNote);
    }
}
