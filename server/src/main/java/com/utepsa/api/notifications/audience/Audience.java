package com.utepsa.api.notifications.audience;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

/**
 * Created by David on 27/07/2016.
 */
@Embeddable
public class Audience {
    @NotEmpty
    @ApiModelProperty(value = "Target to whom it is addressed the notification", required = true)
    @Column(name="target")
    private String target;

    @NotEmpty
    @ApiModelProperty(value = "Type audience (PUBLIC, GROUP OR STUDENT)", required = true)
    @Enumerated(EnumType.STRING)
    @Column(name="type")
    private AudienceType type;

    public Audience(){}

    public Audience(String target, AudienceType type) {
        this.target = target;
        this.type = type;
    }

    @JsonProperty
    public String getTarget() {
        return target;
    }

    @JsonProperty
    public void setTarget(String target) {
        this.target = target;
    }

    @JsonProperty
    public AudienceType getType() {
        return type;
    }

    @JsonProperty
    public void setType(AudienceType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Audience audience = (Audience) o;

        if (type != audience.type) return false;
        return target != null ? target.equals(audience.target) : audience.target == null;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTarget(), getType());
    }

    @Override
    public String toString() {
        return String.format("Audience {target=%s,type=%s}",
                this.target, this.type);
    }
}
