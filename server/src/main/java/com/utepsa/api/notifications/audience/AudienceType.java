package com.utepsa.api.notifications.audience;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum AudienceType {
    @JsonProperty("PUBLIC")
    PUBLIC,
    @JsonProperty("GROUP")
    GROUP,
    @JsonProperty("STUDENT")
    STUDENT
}