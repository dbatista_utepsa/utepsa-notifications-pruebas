package com.utepsa.api.notifications;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.utepsa.api.notifications.audience.Audience;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Table;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;


/**
 * Created by roberto on 19/7/2016.
 */

@Entity
@Table(appliesTo = "Notification")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.api.notifications.Notification.findAll",
                query = "SELECT n FROM Notification n"
        ),
        @NamedQuery(
                name = "com.utepsa.api.notifications.Notification.findByRegisterCode",
                query = "SELECT n FROM Notification n INNER JOIN n.audience a WHERE a.target = 'PUBLIC' OR a.target = :registerCode ORDER BY n.id DESC"
        )
})
@JsonSerialize
@ApiModel(value = "Notification entity", description = "Complete info of a entity Notification")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "The id of the Notification", required = true)
    private long id;

    @NotNull
    @Column
    @ElementCollection(fetch = FetchType.EAGER, targetClass = Audience.class)
    @CollectionTable(name="Audience", joinColumns=@JoinColumn(name="notification_id"))
    @ApiModelProperty(value = "The audience of the Notification", required = true)
    private Set<Audience> audience;

    @NotNull
    @ApiModelProperty(value = "The creationTimestamp of the Notification that give the time in UNIX version", required = true)
    private long creationTimestamp;

    @NotEmpty
    @ApiModelProperty(value = "The title of the Notification", required = true)
    private String title;

    @NotEmpty
    @ApiModelProperty(value = "The content of the Notification", required = true)
    private String content;

    @NotEmpty
    @ApiModelProperty(value = "The type of the Notification that it has the type notice for a while", required = true)
    private String type;

    public Notification() {}

    public Notification(Set<Audience> audience, long creationTimestamp, String title, String content, String type) {
        this.audience = audience;
        this.creationTimestamp = creationTimestamp;
        this.title = title;
        this.content = content;
        this.type = type;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public Set<Audience> getAudience() {
        return audience;
    }

    @JsonProperty
    public void setAudience(Set<Audience> audience) {
        this.audience = audience;
    }

    @JsonProperty
    public long getCreationTimestamp() {
        return creationTimestamp;
    }

    @JsonProperty
    public void setCreationTimestamp(long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    @JsonProperty
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }

    @JsonProperty
    public void setContent(String content) {
        this.content = content;
    }

    @JsonProperty
    public String getType() {
        return type;
    }

    @JsonProperty
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        if (creationTimestamp != that.creationTimestamp) return false;
        if (!audience.equals(that.audience)) return false;
        if (!title.equals(that.title)) return false;
        if (!content.equals(that.content)) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAudience(), getCreationTimestamp(), getTitle(), getContent(), getType());
    }

    @Override
    public String toString() {
            return String.format("Notification {id=%d,audience=%s,creationTimestamp=%s,title=%s,content=%s,type=%s}",
                    this.id, this.audience, this.creationTimestamp, this.title, this.content, this.type);
    }
}
