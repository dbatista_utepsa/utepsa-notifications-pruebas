package com.utepsa.api.documentsStudent;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.util.Objects;
import java.lang.String;
/**
 * Created by shigeots on 28-10-16.
 */

@Entity
@Table(appliesTo = "DocumentsStudent")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.api.documentsStudent.DocumentsStudent.getDocumentsStudentsByRegisterCode",
                query = "SELECT s FROM DocumentsStudent s WHERE registerCode = :registerCode"
        ),
        @NamedQuery(
                name = "com.utepsa.api.documentsStudent.DocumentsStudent.getDocumentStudentByDocumentCode",
                query = "SELECT s FROM DocumentsStudent s WHERE documentCode = :documentCode"
        )
})

@ApiModel(value = "Document of student entity", description = "Complete info of a entity Document of Student")
public class DocumentsStudent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "The id of the Document of student")
    private long id;

    @Column(name = "registerCode", nullable = false)
    @ApiModelProperty(value = "The register code of the Student", required = true)
    private String registerCode;

    @Column(name = "documentCode", nullable = false)
    @ApiModelProperty(value = "The document code of the Student", required = true)
    private String documentCode;

    @Column(name = "dateReceipt", nullable = true)
    @ApiModelProperty(value = "The receipt date of the student document", required = false)
    private String dateReceipt;

    @Column(name = "dateDelivery", nullable = true)
    @ApiModelProperty(value = "The delivery date of the student document", required = false)
    private String dateDelivery;

    @Column(name = "typeDocumentPaper", nullable = false)
    @ApiModelProperty(value = "The type of paper document", required = true)
    private String typeDocumentPaper;

    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the document", required = true)
    private String description;

    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the document", required = true)
    private String state;

    public DocumentsStudent(){}

    public DocumentsStudent(long id, String registerCode, String documentCode, String dateReceipt, String dateDelivery, String typeDocumentPaper, String description, String state) {
        this.id = id;
        this.registerCode = registerCode;
        this.documentCode = documentCode;
        this.dateReceipt = dateReceipt;
        this.dateDelivery = dateDelivery;
        this.typeDocumentPaper = typeDocumentPaper;
        this.description = description;
        this.state = state;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getRegisterCode() {
        return registerCode;
    }

    @JsonProperty
    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    @JsonProperty
    public String getDocumentCode() {
        return documentCode;
    }

    @JsonProperty
    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    @JsonProperty
    public String getDateReceipt() {
        return dateReceipt;
    }

    @JsonProperty
    public void setDateReceipt(String dateReceipt) {
        this.dateReceipt = dateReceipt;
    }

    @JsonProperty
    public String getDateDelivery() {
        return dateDelivery;
    }

    @JsonProperty
    public void setDateDelivery(String dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    @JsonProperty
    public String getTypeDocumentPaper() {
        return typeDocumentPaper;
    }

    @JsonProperty
    public void setTypeDocumentPaper(String typeDocumentPaper) {
        this.typeDocumentPaper = typeDocumentPaper;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty
    public String getState() {
        return state;
    }

    @JsonProperty
    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocumentsStudent)) return false;
        DocumentsStudent documentsStudent = (DocumentsStudent) o;
        return Objects.equals(getId(), documentsStudent.getId()) &&
                Objects.equals(getRegisterCode(), documentsStudent.getRegisterCode()) &&
                Objects.equals(getDocumentCode(), documentsStudent.getDocumentCode()) &&
                Objects.equals(getDateReceipt(), documentsStudent.getDateReceipt()) &&
                Objects.equals(getDateDelivery(), documentsStudent.getDateDelivery()) &&
                Objects.equals(getTypeDocumentPaper(), documentsStudent.getTypeDocumentPaper()) &&
                Objects.equals(getDescription(), documentsStudent.getDescription()) &&
                Objects.equals(getState(), documentsStudent.getState());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRegisterCode(), getDocumentCode(), getDateReceipt(), getDateDelivery(), getTypeDocumentPaper(), getDescription(), getState());
    }

    @Override
    public String toString() {
        return String.format("DocumentsStudent {id=%d,registerCode=%s,documentCode=%s,dateReceipt=%,dateDelivery=%,typeDocumentPaper=%s,description=%s,state=%s}",
                this.id, this.registerCode, this.documentCode, this.dateReceipt, this.dateDelivery, this.typeDocumentPaper, this.description, this.state);

    }

}
