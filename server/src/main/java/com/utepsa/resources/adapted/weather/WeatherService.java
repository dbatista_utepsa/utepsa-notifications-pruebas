package com.utepsa.resources.adapted.weather;

import com.utepsa.adapters.weather.Weather;
import com.utepsa.adapters.weather.WeatherAdapter;
import com.utepsa.config.ExternalServer;

import java.io.IOException;

/**
 * Created by roberto on 24/8/2016.
 */
public class WeatherService {

    private final WeatherAdapter adapter;

    public WeatherService(ExternalServer externalServer) {
        this.adapter = new WeatherAdapter(externalServer);
    }

    public Weather getWeatherFor(String country) throws IOException {
        return this.adapter.getLatestWeatherFor(country);
    }
}
