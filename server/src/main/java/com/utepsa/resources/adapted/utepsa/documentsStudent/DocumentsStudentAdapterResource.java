package com.utepsa.resources.adapted.utepsa.documentsStudent;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by David on 02/11/2016.
 */
@Path("/adapted/utepsa/documentsStudent")
@Produces(MediaType.APPLICATION_JSON)
public class DocumentsStudentAdapterResource {

    private DocumentsStudentAdapterService service;

    public DocumentsStudentAdapterResource(DocumentsStudentAdapterService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/getAll/")
    @ApiOperation( value = "Get and register documents students from Utepsa service")
    public Response getAndRegisterAllStudentsFromUtepsa() {
        try {
            service.registerAllDocumentsStudent();
            return Response.ok().build();
        } catch (Exception exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        }

    }
}
