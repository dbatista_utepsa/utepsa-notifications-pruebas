package com.utepsa.resources.adapted.utepsa.historyNotes;

import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.api.historyNotes.HistoryNotes;
import com.utepsa.api.students.Student;
import com.utepsa.config.ExternalServer;
import com.utepsa.db.historyNotes.HistoryNotesDAO;
import com.utepsa.db.students.StudentDAO;

import java.util.List;

/**
 * Created by David on 13/10/2016.
 */
public class HistoryNotesAdapterService {
    private HistoryNotesDAO historyNotesDAO;
    private HistoryNotesAdapter historyNotesAdapter;
    private StudentDAO studentDAO;

    public HistoryNotesAdapterService(ExternalServer externalServer, HistoryNotesDAO historyNotesDAO, StudentDAO studentDAO) {
        this.historyNotesDAO = historyNotesDAO;
        this.historyNotesAdapter = new HistoryNotesAdapter(externalServer);
        this.studentDAO = studentDAO;
    }

    public void getAndRegisterAllHistoryNotesStudentsFromUtepsa(String studentRegisterCode) throws Exception {
        List<HistoryNotesData> listHistoryNotes = historyNotesAdapter.getAllHistoryNotesStudent(studentRegisterCode);

        if(listHistoryNotes != null){
            for(HistoryNotesData hn: listHistoryNotes){

                if(!historyNotesDAO.existCourseInHistoryNotes(studentRegisterCode, hn.getMat_codigo().trim())){

                    HistoryNotes historyNotes = new HistoryNotes();

                    historyNotes.setRegisterCode(hn.getAlm_registro().trim());
                    historyNotes.setCourseCode(hn.getMat_codigo().trim());
                    historyNotes.setCourseDescription(hn.getMat_descripcion().trim());
                    historyNotes.setMinimunNote(hn.getPln_notaminima());
                    historyNotes.setNote(hn.getNot_nota());
                    historyNotesDAO.create(historyNotes);

                }
            }
        }
    }

    public void getAndRegisterAllHistoryNotes() throws Exception {
        List<Student> listStudent = studentDAO.findAll();
        for(Student student: listStudent){
            this.getAndRegisterAllHistoryNotesStudentsFromUtepsa(student.getAgendaCode());
        }
    }
}
