package com.utepsa.resources.adapted.weather;

import com.utepsa.adapters.weather.Weather;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by roberto on 24/8/2016.
 */
@Path("/adapted/weather")
@Produces(MediaType.APPLICATION_JSON)
public class WeatherResource {

    private final WeatherService service;

    public WeatherResource(WeatherService service) {
        this.service = service;
    }

    @GET
    @Path("{country}")
    public Weather getCountryWeather(@PathParam("country") String country) throws IOException {
        return this.service.getWeatherFor(country);
    }
}
