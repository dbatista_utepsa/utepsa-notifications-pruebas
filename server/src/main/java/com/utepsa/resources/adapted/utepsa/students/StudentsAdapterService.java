package com.utepsa.resources.adapted.utepsa.students;

import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.utepsa.students.StudentData;
import com.utepsa.api.auth.User;
import com.utepsa.api.students.Student;
import com.utepsa.config.ExternalServer;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.users.UserDAO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by David on 11/10/2016.
 */
public class StudentsAdapterService {
    private StudentDAO studentDAO;
    private StudentAdapter studentAdapter;
    private UserDAO userDAO;

    public StudentsAdapterService(ExternalServer externalServer, StudentDAO studentDAO, UserDAO userDAO) {
        this.studentDAO = studentDAO;
        this.studentAdapter = new StudentAdapter(externalServer);
        this.userDAO = userDAO;
    }

    public void getAndRegisterAllStudentsFromUtepsa() throws Exception {
        List<StudentData> listStudent = studentAdapter.getAllStudents();

        if(listStudent != null){
            for(StudentData s: listStudent){
                Student student = new Student();
                student.setName(s.getAgd_nombres().trim());
                student.setFatherLastName(s.getAgd_appaterno().trim());
                student.setMotherLastName(s.getAgd_apmaterno().trim());
                student.setAgendaCode(s.getAgd_codigo().trim());
                if(s.getAgd_fechanac() == null){
                    student.setBirthdate("");
                }
                else{
                    SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date dateTimeZone = formaterTimeZone.parse(s.getAgd_fechanac().trim());

                    SimpleDateFormat formaterShortDate = new SimpleDateFormat("dd/MM/yyyy");
                    student.setBirthdate(formaterShortDate.format(dateTimeZone));
                }
                student.setCareerCode(s.getCrr_codigo().trim());
                student.setDocumentCode(s.getAgd_docnro().trim());
                student.setDocumentType(s.getAgd_docid().trim());
                student.setEmail(s.getCorreo().trim());
                student.setPhoneNumber1(s.getAgd_telf1().trim());
                student.setPhoneNumber2(s.getAgd_telf2().trim());
                student.setGcmId("".trim());
                student.setUserAccount(s.getUsr_login().trim());
                student.setPassword(s.getClave().trim());
                student.setId(0);
                student.setRegisterCode(s.getAlm_registro().trim());

                if(studentDAO.findByDocumentCodeOrRegisterCode(student.getRegisterCode()) == null)
                    studentDAO.create(student);
            }
        }
    }

    public void createUsersFromStudents(){
        List<Student> students = studentDAO.findAll();

        for (Student student: students){
            User user = new User();
            if(student.getPassword().isEmpty()) student.setPassword("Uteps@");
            user.setPassword(student.getPassword());
            user.setAccount(student.getUserAccount());
            user.setRegisterCode(student.getRegisterCode());

            if(userDAO.findByAccountOrRegisterCode(student.getUserAccount()) == null)
                userDAO.create(user);
        }
    }
}
