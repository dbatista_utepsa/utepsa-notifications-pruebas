package com.utepsa.resources.adapted.utepsa.historyNotes;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by David on 13/10/2016.
 */
@Path("/adapted/utepsa/historyNotes")
@Produces(MediaType.APPLICATION_JSON)
public class HistoryNotesAdapterResource {
    private final HistoryNotesAdapterService service;

    public HistoryNotesAdapterResource(HistoryNotesAdapterService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/getHistoryNotes/")
    @ApiOperation( value = "Get and register students from Utepsa service")
    public Response getAndRegisterAllStudentsFromUtepsa() {
        try {
            service.getAndRegisterAllHistoryNotes();
            return Response.ok().build();
        } catch (Exception exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        }

    }
}
