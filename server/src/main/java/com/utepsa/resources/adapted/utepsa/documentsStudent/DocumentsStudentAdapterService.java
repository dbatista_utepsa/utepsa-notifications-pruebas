package com.utepsa.resources.adapted.utepsa.documentsStudent;

import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.api.documentsStudent.DocumentsStudent;
import com.utepsa.api.students.Student;
import com.utepsa.config.ExternalServer;
import com.utepsa.db.documentsStudent.DocumentsStudentDAO;
import com.utepsa.db.students.StudentDAO;

import java.io.IOException;
import java.util.List;

/**
 * Created by David on 02/11/2016.
 */
public class DocumentsStudentAdapterService {
    private DocumentsStudentDAO documentsStudentDAO;
    private DocumentsStudentAdapter documentsStudentAdapter;
    private StudentDAO studentDAO;

    public DocumentsStudentAdapterService(ExternalServer externalServer,DocumentsStudentDAO documentsStudentDAO, StudentDAO studentDAO) {
        this.documentsStudentDAO = documentsStudentDAO;
        this.documentsStudentAdapter = new DocumentsStudentAdapter(externalServer);
        this.studentDAO = studentDAO;
    }

    public void getAndRegisterDocumentsStudentByStudent(String registerCode) throws IOException {
        List<DocumentsStudentData> listDocumentsStudentsData = documentsStudentAdapter.getAllDocumentsStudentForStudent(registerCode);

        for(DocumentsStudentData documentsStudentData : listDocumentsStudentsData ){
            this.createDocumentsStudent(documentsStudentData);
        }
    }

    private void createDocumentsStudent(DocumentsStudentData documentsStudentData){
        if(documentsStudentDAO.getDocumentsStudent(documentsStudentData.getDoc_codigo()) == null){
            DocumentsStudent documentsStudent = new DocumentsStudent(0,documentsStudentData.getAlm_registro(),
                                                                        documentsStudentData.getDoc_codigo(),
                                                                        documentsStudentData.getAlmdoc_fecharecepcion(),
                                                                        documentsStudentData.getAlmdoc_fechaentregar(),
                                                                        documentsStudentData.getAlmdoc_tipopapel(),
                                                                        documentsStudentData.getDoc_descripcion(),
                                                                        documentsStudentData.getESTADO());

            documentsStudentDAO.create(documentsStudent);
        }
    }

    public void registerAllDocumentsStudent() throws IOException {
        List<Student> students = studentDAO.findAll();

        for(Student student : students){
            this.getAndRegisterDocumentsStudentByStudent(student.getRegisterCode());
        }
    }
}
