package com.utepsa.resources.adapted.utepsa.students;

import com.codahale.metrics.annotation.Timed;
import com.utepsa.api.students.Student;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by David on 11/10/2016.
 */
@Path("/adapted/utepsa/students")
@Produces(MediaType.APPLICATION_JSON)
public class StudentsAdapterResource {
    private final StudentsAdapterService studentsAdapterService;

    public StudentsAdapterResource(StudentsAdapterService studentsAdapterService) {this.studentsAdapterService = studentsAdapterService;}

    @GET
    @Timed
    @UnitOfWork
    @Path("/getStudents/")
    @ApiOperation( value = "Get and register students from Utepsa service")
    public Response getAndRegisterAllStudentsFromUtepsa() {
        try {
            studentsAdapterService.getAndRegisterAllStudentsFromUtepsa();
            return Response.ok().build();
        }
        catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (IOException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/createUsers/")
    @ApiOperation( value = "Get and register users from Utepsa service")
    public Response registerUsersFromStudents() {
        try {
            studentsAdapterService.createUsersFromStudents();
            return Response.ok().build();
        }
        catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_FOUND);
        }
    }
}
