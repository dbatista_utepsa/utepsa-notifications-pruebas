package com.utepsa.resources.notifications;

import com.utepsa.adapters.gcm.Data;
import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.adapters.gcm.NotificationGCM;
import com.utepsa.api.notifications.Notification;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.students.StudentDAO;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by DAvid on 22/07/2016.
 */
public class NotificationService {

    private NotificationDAO dao;
    private StudentDAO daoStudent;
    private GCMAdapter adapter;
    private Set<String> ids;
    final private int sizeNotifications = 6;

    public NotificationService(NotificationDAO dao, GCMAdapter gcmAdapter, StudentDAO daoStudent) {
        this.dao = dao;
        this.adapter=gcmAdapter;
        this.daoStudent=daoStudent;
    }

    public List<Notification> getAllNotifications(){
        return dao.findAll();
    }

    public Notification getNotificationById(long id){
        return dao.findById(id);
    }

    public List<Notification> getStudentNotifications(String registerCode, int start_row) {
        return this.dao.findByRegisterCode(registerCode, sizeNotifications, start_row);
    }

    public long createNotification(Notification notification) throws Exception {
        return dao.create(notification);
    }

    public void PushNotificationGcm(String title,String message) throws IOException {
        NotificationGCM notificationGCM= new NotificationGCM();
        String[] idsList = new String[daoStudent.getStudentsGcmId().size()];
        idsList= daoStudent.getStudentsGcmId().toArray(idsList);
        this.ids = new HashSet<>(Arrays.asList(idsList));
        notificationGCM.setData(new Data(title, message));
        notificationGCM.setRegistration_ids(this.ids);
        this.adapter.sendNotification(notificationGCM);
    }
}
