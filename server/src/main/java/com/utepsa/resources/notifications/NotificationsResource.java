package com.utepsa.resources.notifications;


import com.codahale.metrics.annotation.Timed;
import com.utepsa.api.notifications.Notification;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

/**
 * Created by DAvid on 22/07/2016.
 */
@Path(NotificationsResource.SERVICE_PATH)
@Api(value = NotificationsResource.SERVICE_PATH, description = "Operations about Notifications")
@Produces(MediaType.APPLICATION_JSON)
public class NotificationsResource {

    public static final String SERVICE_PATH = "notifications/";
    private final NotificationService service;

    public NotificationsResource(NotificationService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Get all notifications", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of Notifications"),
            @ApiResponse(code = 404, message = "Notifications not found") })
    public List<Notification> getAllNotifications(){
        return service.getAllNotifications();
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a Notification", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 406, message = "Invalid data supplied") })
    public Response createNotification(@ApiParam(value = "JSON format is received with the notification structure.", required = true) Notification notification) {
        try {
            service.PushNotificationGcm(notification.getTitle(),notification.getContent());
            return Response
                    .created(URI.create(SERVICE_PATH + service.createNotification(notification)))
                    .build();
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Find Notification by ID", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid ID supplied"),
            @ApiResponse(code = 404, message = "Notification not found") })
    public Notification getNotificationById(@PathParam("id") LongParam id) {
        try {
            return service.getNotificationById(id.get());
        } catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        }
    }
}