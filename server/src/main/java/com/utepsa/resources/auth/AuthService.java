package com.utepsa.resources.auth;

import com.utepsa.api.auth.User;
import com.utepsa.api.students.Student;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.users.UserDAO;

/**
 * Created by david on 19/10/16.
 */
public class AuthService {
    private final UserDAO userDAO;
    private final StudentDAO studentDAO;

    public AuthService(UserDAO userDAO, StudentDAO studentDAO) {
        this.userDAO = userDAO;
        this.studentDAO = studentDAO;
    }

    public Student login(String accountOrRegisterCode, String password) throws Exception {
        User user = userDAO.findByAccountOrRegisterCodeWithPassword(accountOrRegisterCode, password);

        if(user == null)
            throw new Exception("User does not exist",new Throwable("User or Password incorrect").getCause());

        Student student = this.studentDAO.findByDocumentCodeOrRegisterCode(user.getRegisterCode());

        if(student == null)
            throw new Exception("A student can not find the registration code",new Throwable("Error finding student when Login").getCause());

        student.setPassword("");

        return student;
    }
}
