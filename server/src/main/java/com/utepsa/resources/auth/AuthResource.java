package com.utepsa.resources.auth;

import com.codahale.metrics.annotation.Timed;
import com.utepsa.api.students.Student;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by david on 5/8/16.
 */
@Path(AuthResource.SERVICE_PATH)
@Produces(MediaType.APPLICATION_JSON)

public class AuthResource {
    public static final String SERVICE_PATH = "auth/";

    private final AuthService service;

    public AuthResource(AuthService service) {
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces("application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 406, message = "User does not exist") })
    public Response loginUser(@HeaderParam("accountOrRegisterCode") String accountOrRegisterCode,
                            @HeaderParam("password") String password) {
        try {
            Student student = service.login(accountOrRegisterCode, password);
            return Response.ok(student, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }
}
