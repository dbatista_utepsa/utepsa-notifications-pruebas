package com.utepsa.resources.documentsStudent;

import com.utepsa.api.documentsStudent.DocumentsStudent;
import com.utepsa.db.documentsStudent.DocumentsStudentDAO;

import java.util.List;

/**
 * Created by shigeots on 31-10-16.
 */
public class DocumentsStudentService {

    private final DocumentsStudentDAO dao;

    public DocumentsStudentService(DocumentsStudentDAO dao) {
        this.dao = dao;
    }

    public List<DocumentsStudent> getDocumentsStudentByRegisterCode(String registerCode) {
        return this.dao.getDocumentsStudentsByRegisterCode(registerCode);
    }

    public long createDocumentsStudent(DocumentsStudent documentsStudent) throws Exception {
        return dao.create(documentsStudent);
    }
}
