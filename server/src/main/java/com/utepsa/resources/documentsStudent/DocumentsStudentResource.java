package com.utepsa.resources.documentsStudent;


import com.codahale.metrics.annotation.Timed;
import com.utepsa.api.documentsStudent.DocumentsStudent;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by shigeots on 31-10-16.
 */

@Path(DocumentsStudentResource.SERVICE_PATH)
@Api(value = DocumentsStudentResource.SERVICE_PATH, description = "Operations about DocumentsStudent")
@Produces(MediaType.APPLICATION_JSON)
public class DocumentsStudentResource {

    public static final String SERVICE_PATH = "documentsStudent/";

    private final DocumentsStudentService service;

    public DocumentsStudentResource(DocumentsStudentService service) {
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{registerCode}")
    @ApiOperation( value = "Find DocumentsStudent by register code", response = DocumentsStudent.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Valid register code student found"),
            @ApiResponse(code = 400, message = "Invalid register code supplied"),
            @ApiResponse(code = 404, message = "Student not found"),
            @ApiResponse(code = 204, message = "Student without content"),
            @ApiResponse(code = 422, message = "Invalid register code")})
    public List <DocumentsStudent> getDocumentsStudentByRegisterCode (@ApiParam(value = "register code of Student that needs to be fetched", required = true)@PathParam("registerCode") String registerCode) {
        try {
            return service.getDocumentsStudentByRegisterCode (registerCode);
        }
        catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        }
    }


}
