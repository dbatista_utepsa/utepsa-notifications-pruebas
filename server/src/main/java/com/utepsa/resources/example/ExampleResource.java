package com.utepsa.resources.example;

import com.utepsa.api.example.ExampleMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by roberto on 7/7/2016.
 */
@Path("example")
@Api(value = "/example", description = "Operations about Example")
@Produces(MediaType.APPLICATION_JSON)
public class ExampleResource {

    private ExampleService service;

    public ExampleResource(ExampleService service) {
        this.service = service;
    }

    @GET
    @ApiOperation(value = "Just an Example", response = ExampleMessage.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid ID supplied"),
            @ApiResponse(code = 404, message = "Example not found") })
    public ExampleMessage simpleAnswer() {
        return service.simpleAnswer();
    }
}
