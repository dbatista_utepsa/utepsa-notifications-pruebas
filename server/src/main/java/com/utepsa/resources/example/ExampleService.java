package com.utepsa.resources.example;

import com.utepsa.api.example.ExampleMessage;

import java.util.Random;

/**
 * Created by roberto on 7/7/2016.
 */
public class ExampleService {

    private String serverName;

    private final String MESSAGE = "Simple Message";
    private final Random intGenerator = new Random();

    public ExampleService(String serverName) {
        this.serverName = serverName;
    }

    public ExampleMessage simpleAnswer() {
        return new ExampleMessage(intGenerator.nextInt(), MESSAGE, this.serverName);
    }
}
