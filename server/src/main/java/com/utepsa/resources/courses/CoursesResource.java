package com.utepsa.resources.courses;

import com.codahale.metrics.annotation.Timed;
import com.utepsa.api.courses.Course;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by JoseCarlos on 28-07-16.
 */
@Path(CoursesResource.SERVICE_PATH)
@Api(value = CoursesResource.SERVICE_PATH, description = "Operations about Courses")
@Produces(MediaType.APPLICATION_JSON)
public class CoursesResource {
    public static final String SERVICE_PATH="courses/";
    private final CoursesService service;

    public CoursesResource(CoursesService service){
        this.service=service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{code}")
    @ApiOperation( value = "Find Course by Code", response = Course.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Course Found"),
            @ApiResponse(code = 404, message = "There is no a Course with Code supplied")})
    public Course getCourseByCode (
            @ApiParam(value = "Course Code, example: 581", required= true)
            @PathParam("code") LongParam code)
            throws NotFoundException
    {
        return service.getByCode(code.get());
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation(value = "Find all Courses", response = Course.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of Courses"),
            @ApiResponse(code = 404, message = "Courses not found") })
    public List<Course> getAllCourses() {
        return service.getAllCourses();
    }
}
