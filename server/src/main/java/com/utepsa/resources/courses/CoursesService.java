package com.utepsa.resources.courses;

import com.utepsa.api.courses.Course;
import com.utepsa.db.courses.CourseDAO;

import javax.ws.rs.NotFoundException;
import java.util.List;

/**
 * Created by Roberto Perez on 28-07-16.
 */

public class CoursesService {

    private final CourseDAO dao;

    public CoursesService(CourseDAO dao){
        this.dao = dao;
    }

    public Course getByCode(long code) throws NotFoundException {
        Course course = this.dao.findByCode(code);
        if (course != null) {
            return course;
        }
        throw new NotFoundException("There is no a Course with Code supplied");
    }

    public List<Course> getAllCourses(){
        return dao.getAll();
    }
}
