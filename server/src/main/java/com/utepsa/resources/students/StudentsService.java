package com.utepsa.resources.students;

import com.utepsa.api.historyNotes.HistoryNotes;
import com.utepsa.api.students.Student;
import com.utepsa.db.historyNotes.HistoryNotesDAO;
import com.utepsa.db.students.StudentDAO;

import javax.ws.rs.WebApplicationException;
import java.util.List;

/**
 * Created by roberto on 12/7/2016.
 */
public class StudentsService {


    private final StudentDAO dao;
    private final HistoryNotesDAO historyNotesDao;

    public StudentsService(StudentDAO dao, HistoryNotesDAO historyNotesDao) {
        this.dao = dao;
        this.historyNotesDao = historyNotesDao;
    }

    public List<Student> getAllStudents() {return dao.findAll();}

    private boolean isValidCi(int ci) {
        if (ci < 0) return false;
        int ciLength = String.valueOf(ci).length();
        return ciLength >= Student.CI_MIN_LENGTH;
    }

    public long createStudent(Student student) throws Exception {
        if(student.getDocumentCode().length() < 6) throw new Exception("Negative values are not accepted in the document code.");
        if(student.getDocumentCode().length() > 20) throw new Exception("The document code must not be less than 20 digits.");
        if(dao.findByDocumentCodeOrRegisterCode(student.getRegisterCode()) != null) throw new Exception("Student already exists.");

        return dao.create(student);
    }

    public Student getStudentByDocumentCodeOrRegisterCode (String documentCodeOrRegisterCode) {
        if(documentCodeOrRegisterCode.length() < 6)
            throw new WebApplicationException(422);
        return this.dao.findByDocumentCodeOrRegisterCode(documentCodeOrRegisterCode);
    }

    public boolean registerGcmIdStudent(String registerCode, String gcmId) throws Exception {
        return dao.registerGcmId(registerCode, gcmId);
    }

    public List<HistoryNotes> getHistoryNotesByStudent(String registerCode) throws Exception {
        return this.historyNotesDao.getHistoryNotesByStudent(registerCode);
    }
}
