package com.utepsa.resources.students;

import com.codahale.metrics.annotation.Timed;
import com.utepsa.api.historyNotes.HistoryNotes;
import com.utepsa.api.notifications.Notification;
import com.utepsa.api.students.Student;
import com.utepsa.resources.notifications.NotificationService;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.dropwizard.jersey.params.NonEmptyStringParam;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.util.List;
/**
 * Created by roberto on 12/7/2016.
 */
@Path(StudentsResource.SERVICE_PATH)
@Api(value = StudentsResource.SERVICE_PATH, description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class StudentsResource {
    public static final String SERVICE_PATH = "students/";

    private final StudentsService service;
    private final NotificationService serviceNotifications;

    public StudentsResource(StudentsService service, NotificationService serviceNotifications) {
        this.service = service;
        this.serviceNotifications = serviceNotifications;
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a Student", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Student successfully created"),
            @ApiResponse(code = 400, message = "Invalid data supplied"),
            @ApiResponse(code = 406, message = "The CI must not be less than 7 digits"),})
    public Response createStudent(Student student) {
        try {
            return Response
                    .created(URI.create(SERVICE_PATH + service.createStudent(student)))
                    .build();
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @ApiOperation( value = "Find all Student", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All students found"),
            @ApiResponse(code = 404, message = "Students not found"),
            @ApiResponse(code = 204, message = "Students without content")})
    public List<Student> getAllStudents() {
        return service.getAllStudents();
    }

    /* Students Notifications */
    @GET
    @Timed
    @UnitOfWork
    @Path("/{registerCode}/notifications/{numberRow}")
    @ApiOperation( value = "Students get notifications", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Valid register code notification found"),
            @ApiResponse(code = 404, message = "Notifications not found"),
            @ApiResponse(code = 204, message = "Notifications without content")})
    public List<Notification> getStudentNotificationsByRegisterCode(
            @ApiParam(value = "Registry code Student that needs to be fetched", required = true)
            @PathParam("registerCode") NonEmptyStringParam value,
            @ApiParam(value = "Row number to get", required = true)
            @PathParam("numberRow") int numRow) {
                String registerCode = value.get().orElse("none");
                return serviceNotifications.getStudentNotifications(registerCode, numRow);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{documentCodeOrRegisterCode}")
    @ApiOperation( value = "Find Student by document code or register code", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Valid CI or register code student found"),
            @ApiResponse(code = 400, message = "Invalid CI or register code supplied"),
            @ApiResponse(code = 404, message = "Student not found"),
            @ApiResponse(code = 204, message = "Student without content"),
            @ApiResponse(code = 422, message = "Invalid CI or register code")})
    public Student getStudentByDocumentCodeOrRegisterCode(@ApiParam(value = "CI or register code of Student that needs to be fetched", required = true)@PathParam("documentCodeOrRegisterCode") String documentCodeOrRegisterCode) {
        try {
            return service.getStudentByDocumentCodeOrRegisterCode(documentCodeOrRegisterCode);
        }
        catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        }
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/registerGcmId/{registerCode}/{gcmId}")
    @ApiOperation( value = "Register GCM id Student", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Register Successful"),
            @ApiResponse(code = 406, message = "Invalid data supplied")})
    public Response registerGcmIdStudent(@PathParam("registerCode") String registerCode, @PathParam("gcmId") String gcmId) {
        try {
            service.registerGcmIdStudent(registerCode, gcmId);
            return Response.ok().build();
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("{registerCode}/historyNotes")
    @ApiOperation( value = "Find the History Notes of a Student with his RegisterCode", response = HistoryNotes.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Valid register code"),
            @ApiResponse(code = 400, message = "Invalid register code supplied"),
            @ApiResponse(code = 404, message = "History Notes not found"),
            @ApiResponse(code = 204, message = "History Notes without content"),
            @ApiResponse(code = 422, message = "Invalid register code")})
    public List<HistoryNotes> getHistoryNotesByStudent(@PathParam("registerCode") String registerCode) {
        try {
            return service.getHistoryNotesByStudent(registerCode);
        } catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_FOUND);
        }
    }
}
