package com.utepsa.adapters.utepsa.students;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by david on 4/10/16.
 */
public class StudentData {

    private String agd_codigo;
    private String agd_appaterno;
    private String agd_apmaterno;
    private String agd_nombres;
    private String agd_fechanac;
    private String agd_docid;
    private String agd_docnro;
    private String agd_telf1;
    private String agd_telf2;
    private String crr_codigo;
    private String alm_registro;
    private String usr_login;
    private String clave;
    private String correo;

    public StudentData() {
    }

    public StudentData(String agd_codigo, String agd_appaterno, String agd_apmaterno, String agd_nombres, String agd_fechanac, String agd_docid, String agd_docnro, String agd_telf1, String agd_telf2, String crr_codigo, String alm_registro, String usr_login, String clave, String correo) {
        this.agd_codigo = agd_codigo;
        this.agd_appaterno = agd_appaterno;
        this.agd_apmaterno = agd_apmaterno;
        this.agd_nombres = agd_nombres;
        this.agd_fechanac = agd_fechanac;
        this.agd_docid = agd_docid;
        this.agd_docnro = agd_docnro;
        this.agd_telf1 = agd_telf1;
        this.agd_telf2 = agd_telf2;
        this.crr_codigo = crr_codigo;
        this.alm_registro = alm_registro;
        this.usr_login = usr_login;
        this.clave = clave;
        this.correo = correo;
    }

    @JsonProperty
    public String getAgd_codigo() {
        return agd_codigo;
    }

    @JsonProperty
    public void setAgd_codigo(String agd_codigo) {
        this.agd_codigo = agd_codigo;
    }

    @JsonProperty
    public String getAgd_appaterno() {
        return agd_appaterno;
    }

    @JsonProperty
    public void setAgd_appaterno(String agd_appaterno) {
        this.agd_appaterno = agd_appaterno;
    }

    @JsonProperty
    public String getAgd_apmaterno() {
        return agd_apmaterno;
    }

    @JsonProperty
    public void setAgd_apmaterno(String agd_apmaterno) {
        this.agd_apmaterno = agd_apmaterno;
    }

    @JsonProperty
    public String getAgd_nombres() {
        return agd_nombres;
    }

    @JsonProperty
    public void setAgd_nombres(String agd_nombres) {
        this.agd_nombres = agd_nombres;
    }

    @JsonProperty
    public String getAgd_fechanac() {
        return agd_fechanac;
    }

    @JsonProperty
    public void setAgd_fechanac(String agd_fechanac) {
        this.agd_fechanac = agd_fechanac;
    }

    @JsonProperty
    public String getAgd_docid() {
        return agd_docid;
    }

    @JsonProperty
    public void setAgd_docid(String agd_docid) {
        this.agd_docid = agd_docid;
    }

    @JsonProperty
    public String getAgd_docnro() {
        return agd_docnro;
    }

    @JsonProperty
    public void setAgd_docnro(String agd_docnro) {
        this.agd_docnro = agd_docnro;
    }

    @JsonProperty
    public String getAgd_telf1() {
        return agd_telf1;
    }

    @JsonProperty
    public void setAgd_telf1(String agd_telf1) {
        this.agd_telf1 = agd_telf1;
    }

    @JsonProperty
    public String getAgd_telf2() {
        return agd_telf2;
    }

    @JsonProperty
    public void setAgd_telf2(String agd_telf2) {
        this.agd_telf2 = agd_telf2;
    }

    @JsonProperty
    public String getCrr_codigo() {
        return crr_codigo;
    }

    @JsonProperty
    public void setCrr_codigo(String crr_codigo) {
        this.crr_codigo = crr_codigo;
    }

    @JsonProperty
    public String getAlm_registro() {
        return alm_registro;
    }

    @JsonProperty
    public void setAlm_registro(String alm_registro) {
        this.alm_registro = alm_registro;
    }

    @JsonProperty
    public String getUsr_login() {
        return usr_login;
    }

    @JsonProperty
    public void setUsr_login(String usr_login) {
        this.usr_login = usr_login;
    }

    @JsonProperty
    public String getClave() {
        return clave;
    }

    @JsonProperty
    public void setClave(String clave) {
        this.clave = clave;
    }

    @JsonProperty
    public String getCorreo() {
        return correo;
    }

    @JsonProperty
    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
