package com.utepsa.adapters.utepsa.documentsStudent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by david on 28/10/16.
 */
public class DocumentsStudentAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public DocumentsStudentAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/StudentDocuments").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    private String buildRequestUrl(String registerCode) {
        return String.format("%sStudentDocuments/%s",
                this.externalServer.getUrl(),
                registerCode);
    }

    public List<DocumentsStudentData> getAllDocumentsStudent() throws IOException{
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<DocumentsStudentData> listDocumentsStudentData;
            listDocumentsStudentData = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<DocumentsStudentData>>(){});

            return listDocumentsStudentData;
        }
        return null;
    }

    public List<DocumentsStudentData> getAllDocumentsStudentForStudent(String registerCode) throws IOException{
        String url = this.buildRequestUrl(registerCode);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<DocumentsStudentData> listDocumentsStudentData;
            listDocumentsStudentData = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<DocumentsStudentData>>(){});

            return listDocumentsStudentData;
        }
        return null;
    }
}
