package com.utepsa.adapters.utepsa.historyNotes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by David on 11/10/2016.
 */
public class HistoryNotesAdapter {
    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public HistoryNotesAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/Student/0000000018/historyNotes").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    public List<HistoryNotesData> getAllHistoryNotesStudent(String agendCode) throws IOException{
        String url = this.buildRequestUrl(agendCode);
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<HistoryNotesData> listHistoryNotes;
            listHistoryNotes = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<HistoryNotesData>>(){});

            return listHistoryNotes;
        }
        return null;
    }

    private String buildRequestUrl(String registerCode) {
        return String.format("%sStudent/%s/historyNotes",
                this.externalServer.getUrl(),
                registerCode);
    }
}
