package com.utepsa.adapters.utepsa.students;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by david on 4/10/16.
 */
public class StudentAdapter {

    private final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private ExternalServer externalServer;

    public StudentAdapter(ExternalServer externalServer) {
        this.externalServer = externalServer;
    }

    private HttpResponse doRequestTo(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        return client.execute(request);
    }

    public boolean isServerUp() throws IOException {
        return this.doRequestTo(externalServer.getUrl()+"/Student").getStatusLine()
                .getStatusCode() == HttpStatus.SC_OK;
    }

    public List<StudentData> getAllStudents() throws IOException{
        String url = this.buildRequestUrl("");
        HttpResponse response = this.doRequestTo(url);

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<StudentData> listStudents;
            listStudents = MAPPER.readValue(response.getEntity().getContent(), new TypeReference<List<StudentData>>(){});

            return listStudents;
        }
        return null;
    }

    private String buildRequestUrl(String param) {
        return String.format("%s/Student/%s",
                this.externalServer.getUrl(),
                param);
    }
}
