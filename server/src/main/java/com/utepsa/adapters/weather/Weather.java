package com.utepsa.adapters.weather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * Created by roberto on 24/8/2016.
 *
 * This class is just a siple POJO based in the response from
 * http://api.apixu.com/v1/current.json?key=be57df18f3ae419581d224836162408&q=Bolivia
 */
public class Weather {

    private Location location;
    private Current current;

    public Weather() { }

    @JsonProperty
    public Location getLocation() {
        return location;
    }

    @JsonProperty
    public void setLocation(Location location) {
        this.location = location;
    }

    @JsonProperty
    public Current getCurrent() {
        return current;
    }

    @JsonProperty
    public void setCurrent(Current current) {
        this.current = current;
    }
}

@JsonIgnoreProperties( value = {
    "lat",
    "lon",
    "tz_id",
    "localtime_epoch",
    "localtime" } )
class Location {
    private String name;
    private String region;
    private String country;

    public Location() { }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getRegion() {
        return region;
    }

    @JsonProperty
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty
    public String getCountry() {
        return country;
    }

    @JsonProperty
    public void setCountry(String country) {
        this.country = country;
    }
}

@JsonIgnoreProperties( value = {
        "last_updated_epoch",
        "is_day",
        "condition",
        "wind_mph",
        "wind_kph",
        "wind_degree",
        "wind_dir",
        "pressure_mb",
        "pressure_in",
        "precip_mm",
        "precip_in",
        "humidity",
        "cloud",
        "feelslike_c",
        "feelslike_f" } )
class Current {
    private double temp_c;
    private double temp_f;
    private String last_updated;

    public Current() { }

    @JsonProperty
    public double getTemp_c() {
        return temp_c;
    }

    @JsonProperty
    public void setTemp_c(double temp_c) {
        this.temp_c = temp_c;
    }

    @JsonProperty
    public double getTemp_f() {
        return temp_f;
    }

    @JsonProperty
    public void setTemp_f(double temp_f) {
        this.temp_f = temp_f;
    }

    @JsonProperty
    public String getLast_updated() {
        return last_updated;
    }

    @JsonProperty
    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }
}
