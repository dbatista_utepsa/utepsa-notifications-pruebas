package com.utepsa.adapters.gcm;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto on 25/8/2016.
 */
public class Data {
    private String title;
    private String message;

    public Data() { }

    public Data(String title, String message) {
        this.title = title;
        this.message = message;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    @JsonProperty
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    @JsonProperty
    public void setMessage(String message) {
        this.message = message;
    }
}
