package com.utepsa.adapters.gcm;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * Created by roberto on 25/8/2016.
 */
public class NotificationGCM {

    private Data data;
    private Set<String> registration_ids;

    public NotificationGCM(){ }

    public NotificationGCM(Data data, Set<String> registration_ids) {
        this.data = data;
        this.registration_ids = registration_ids;
    }

    @JsonProperty
    public Data getData() {
        return data;
    }

    @JsonProperty
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty
    public Set<String> getRegistration_ids() {
        return registration_ids;
    }

    @JsonProperty
    public void setRegistration_ids(Set<String> registration_ids) {
        this.registration_ids = registration_ids;
    }
}
