package com.utepsa.adapters.gcm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.utepsa.config.ExternalServer;
import io.dropwizard.jackson.Jackson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by roberto on 25/8/2016.
 */
public class GCMAdapter {

    final private ExternalServer server;

    private final ObjectMapper MAPPER = Jackson.newObjectMapper();

    public GCMAdapter(ExternalServer server) {
        this.server = server;
    }

    public int sendNotification(NotificationGCM notification) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(this.server.getUrl());
        request.setHeader("Authorization", "key=" + this.server.getKey());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        StringEntity entity = new StringEntity(MAPPER.writeValueAsString(notification));
        request.setEntity(entity);

        return client.execute(request).getStatusLine().getStatusCode();
    }
}
