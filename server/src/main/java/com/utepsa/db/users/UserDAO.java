package com.utepsa.db.users;

import com.utepsa.api.auth.User;

/**
 * Created by david on 19/10/16.
 */
public interface UserDAO {
    User findByAccountOrRegisterCodeWithPassword(String accountOrRegisterCode, String password);
    long create(User user);
    User findByAccountOrRegisterCode(String accountOrRegisterCode);
}
