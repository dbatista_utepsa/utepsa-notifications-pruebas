package com.utepsa.db.users;

import com.utepsa.api.auth.User;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by david on 19/10/16.
 */
public class UserRealDAO extends AbstractDAO<User> implements UserDAO {

    public UserRealDAO (SessionFactory factory) {super(factory);}

    public long create(User user){
        return persist(user).getId();
    }

    @Override
    public User findByAccountOrRegisterCodeWithPassword(String accountOrRegisterCode, String password) {
        return  uniqueResult(namedQuery("com.utepsa.api.auth.User.loginByAccountOrRegisterCode").setParameter("accountOrRegisterCode", accountOrRegisterCode).setParameter("password", password));
    }

    @Override
    public User findByAccountOrRegisterCode(String accountOrRegisterCode){
        return uniqueResult(namedQuery("com.utepsa.api.auth.User.findByAccountOrRegisterCode").setParameter("accountOrRegisterCode", accountOrRegisterCode));
    }
}