package com.utepsa.db.users;

import com.utepsa.api.auth.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by david on 19/10/16.
 */
public class UserFakeDAO implements UserDAO{
    final private Map<Long, User> userTable = new HashMap<>();

    public UserFakeDAO() {
        User user = new User(1,"dbatista","403202","123456");
        userTable.put(user.getId(), user);
    }

    @Override
    public User findByAccountOrRegisterCodeWithPassword(String accountOrRegisterCode, String password) {
        for (Map.Entry<Long, User> user : this.userTable.entrySet()) {
            if((user.getValue().getRegisterCode().equals(accountOrRegisterCode) || user.getValue().getAccount().equals(accountOrRegisterCode)) && user.getValue().getPassword().equals(password))
                return this.userTable.get(user.getKey());
        }

        return null;
    }

    @Override
    public long create(User user) {
        userTable.put(user.getId(), user);
        return user.getId();
    }

    @Override
    public User findByAccountOrRegisterCode(String accountOrRegisterCode) {
        for (Map.Entry<Long, User> user : this.userTable.entrySet()) {
            if((user.getValue().getRegisterCode().equals(accountOrRegisterCode) || user.getValue().getAccount().equals(accountOrRegisterCode)))
                return this.userTable.get(user.getKey());
        }

        return null;
    }
}
