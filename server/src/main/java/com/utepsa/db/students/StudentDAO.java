package com.utepsa.db.students;

import com.utepsa.api.students.Student;

import java.util.List;

/**
 * Created by shigeo on 01-09-16.
 */
public interface StudentDAO {
    Student findByDocumentCode (String documentCode);

    long create(Student student);

    List<Student> findAll();

    Student findByDocumentCodeOrRegisterCode (String documentCodeOrRegisterCode);

    Student findByUserAccountOrRegisterCode (String userAccountOrRegisterCode);

    boolean registerGcmId(String registerCode, String gcmId);

    List<Student> getStudentsGcmId();
}
