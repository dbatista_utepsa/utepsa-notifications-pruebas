package com.utepsa.db.students;

import com.utepsa.api.students.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by shigeo on 01-09-16.
 */
public class StudentFakeDAO implements StudentDAO {
    final private Map<Long, Student> studentTable = new HashMap<>();
    final private AtomicLong idGenerator = new AtomicLong(1);

    public StudentFakeDAO() {
        Student student = new Student(1,"403202","david","batista","taboada","30/01/02","CI","8914234","123456","","1","403202","dbatista","123456","","");
        studentTable.put(student.getId(),student);
    }

    @Override
    public Student findByDocumentCode(String documentCode) {
        return this.studentTable.get(documentCode);
    }

    @Override
    public long create(Student student) {
        student.setId(idGenerator.getAndIncrement());
        this.studentTable.put(student.getId(), student);
        return student.getId();
    }

    @Override
    public List<Student> findAll(){
        return new ArrayList<>(this.studentTable.values());
    }

    @Override
    public Student findByDocumentCodeOrRegisterCode (String documentCodeOrRegisterCode) {
        for (Map.Entry<Long, Student> entry : studentTable.entrySet()) {
            if(entry.getValue().getDocumentCode().equals(documentCodeOrRegisterCode)  || entry.getValue().getRegisterCode().equals(documentCodeOrRegisterCode))
                return this.studentTable.get(entry.getKey());
        }

        return null;
    }

    @Override
    public Student findByUserAccountOrRegisterCode (String userAccountOrRegisterCode) {
        for (Map.Entry<Long, Student> entry : studentTable.entrySet()) {
            if(entry.getValue().getUserAccount().equals(userAccountOrRegisterCode)  || entry.getValue().getRegisterCode().equals(userAccountOrRegisterCode))
                return this.studentTable.get(entry.getKey());
        }

        return null;
    }

    @Override
    public boolean registerGcmId(String registerCode, String gcmId) {
        return true;
    }

    @Override
    public List<Student> getStudentsGcmId() {
        return  new ArrayList<>(this.studentTable.values());
    }
}
