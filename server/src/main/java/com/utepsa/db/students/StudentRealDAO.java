package com.utepsa.db.students;

import com.utepsa.api.students.Student;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by shigeo on 01-09-16.
 */
public class StudentRealDAO extends AbstractDAO<Student> implements StudentDAO {

    public StudentRealDAO (SessionFactory factory) {super(factory);}

    @Override
    public Student findByDocumentCode(String documentCode) {
        return uniqueResult(namedQuery("com.utepsa.api.students.Student.findByDocumentCode").setParameter("documentCode", documentCode));
    }

    @Override
    public long create(Student student) {
        final Student created = persist(student);
        if(created == null) try {
            throw new Exception("The student was not created.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return created.getId();
    }

    @Override
    public List<Student> findAll() {
        return list(namedQuery("com.utepsa.api.students.Student.findAll"));
    }

    @Override
    public Student findByDocumentCodeOrRegisterCode(String documentCodeOrRegisterCode) {
        return  uniqueResult(namedQuery("com.utepsa.api.students.Student.findByDocumentCodeOrRegisterCode").setParameter("documentCodeOrRegisterCode", documentCodeOrRegisterCode));
    }

    @Override
    public Student findByUserAccountOrRegisterCode(String userAccountOrRegisterCode) {
        return  uniqueResult(namedQuery("com.utepsa.api.students.Student.findByUserAccountOrRegisterCode").setParameter("userAccountOrRegisterCode", userAccountOrRegisterCode));
    }

    @Override
    public boolean registerGcmId(String registerCode, String gcmId) {
        Student student = this.findByUserAccountOrRegisterCode(registerCode);
        student.setGcmId(gcmId);
        persist(student);
        return true;
    }

    @Override
    public List<Student> getStudentsGcmId() {
        return list(namedQuery("com.utepsa.api.students.Student.getStudentsGcmId"));
    }
}
