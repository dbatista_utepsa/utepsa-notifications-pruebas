package com.utepsa.db.courses;

import com.utepsa.api.courses.Course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by roberto on 10/8/2016.
 */
public class CourseFakeDAO implements CourseDAO {
    final private Map<Long, Course> courseTable = new HashMap<>();

    @Override
    public Course findByCode(long code) {
        return this.courseTable.get(code);
    }

    @Override
    public long create(Course course) {
        this.courseTable.put(course.getCode(), course);
        return course.getCode();
    }

    @Override
    public List<Course> getAll() {
        return new ArrayList<>(this.courseTable.values());
    }
}
