package com.utepsa.db.courses;

import com.utepsa.api.courses.Course;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Roberto Perez on 28-07-16.
 */
public class CourseRealDAO extends AbstractDAO<Course> implements CourseDAO {

    public CourseRealDAO(SessionFactory factory) {
        super(factory);
    }

    @Override
    public Course findByCode(long code) {
        return get(code);
    }

    @Override
    public long create(Course course) {
        return persist(course).getCode();
    }

    @Override
    public List<Course> getAll(){
        return list(namedQuery("com.utepsa.api.courses.Course.findAll"));
    }


}
