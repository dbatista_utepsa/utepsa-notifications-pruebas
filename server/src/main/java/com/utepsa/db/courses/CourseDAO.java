package com.utepsa.db.courses;

import com.utepsa.api.courses.Course;

import java.util.List;

/**
 * Created by Roberto Perez on 10/8/2016.
 */
public interface CourseDAO {
    Course findByCode(long code);

    long create(Course course);

    List<Course> getAll();
}
