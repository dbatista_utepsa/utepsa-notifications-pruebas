package com.utepsa.db.historyNotes;

import com.utepsa.api.historyNotes.HistoryNotes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexis Ardaya on 10/12/2016.
 */
public class HistoryNotesFakeDAO implements HistoryNotesDAO {
    final private Map<Long, HistoryNotes> historyNotesTable = new HashMap<>();

    @Override
    public List<HistoryNotes> getHistoryNotesByStudent(String registerCode) {
       return new ArrayList<>(this.historyNotesTable.values());
    }

    @Override
    public long create(HistoryNotes historyNotes) throws Exception {
        return 0;
    }

    @Override
    public boolean existCourseInHistoryNotes(String studentAgendCode, String courseCode) {
        return false;
    }

}
