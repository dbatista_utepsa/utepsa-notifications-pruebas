package com.utepsa.db.historyNotes;

import com.utepsa.api.historyNotes.HistoryNotes;

import java.util.List;

/**
 * Created by Alexis Ardaya on 10/12/2016.
 */
public interface HistoryNotesDAO {

    List<HistoryNotes> getHistoryNotesByStudent(String registerCode);
    long create(HistoryNotes historyNotes) throws Exception;
    boolean existCourseInHistoryNotes(String studentAgendCode, String courseCode);
}
