package com.utepsa.db.historyNotes;

import com.utepsa.api.historyNotes.HistoryNotes;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Alexis Ardaya on 10/12/2016.
 */
public class HistoryNotesRealDAO extends AbstractDAO<HistoryNotes> implements HistoryNotesDAO {

    public HistoryNotesRealDAO(SessionFactory factory) {
        super(factory);
    }

    @Override
    public List<HistoryNotes> getHistoryNotesByStudent(String registerCode) {
        return list(namedQuery("com.utepsa.api.historyNotes.historyNotes.historyNotesByStudent").setParameter("registerCode",registerCode));
    }

    public long create(HistoryNotes historyNotes) throws Exception{
        final HistoryNotes created = persist(historyNotes);
        if(created == null) throw new Exception("The notification was not created.");
        return created.getId();
    }

    public boolean existCourseInHistoryNotes(String studentRegisterCode, String courseCode){
        if(uniqueResult(namedQuery("com.utepsa.api.historyNotes.historyNotes.historyNotesByStudentAndCourse").setParameter("studentRegisterCode", studentRegisterCode).setParameter("courseCode", courseCode)) == null)
            return false;
        else
            return true;
    }

}
