package com.utepsa.db.notification;

import com.utepsa.api.notifications.Notification;
import com.utepsa.api.notifications.audience.Audience;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shigeots on 03-11-16.
 */
public class NotificationFakeDAO implements NotificationDAO {
    final private Map<Long, Notification> notificationTable = new HashMap<>();


    @Override
    public Notification findById(long id) {
        for (Map.Entry<Long, Notification> notification : this.notificationTable.entrySet()) {
            if(notification.getValue().getId() == id)
                return this.notificationTable.get(notification.getKey());
        }
        return null;
    }

    @Override
    public List<Notification> findAll() {
        return new ArrayList<>(this.notificationTable.values());
    }

    @Override
    public List<Notification> findByRegisterCode(String registerCode, int pageSize, int startRow) {
        List<Notification> result = new ArrayList<>();

        for (Map.Entry<Long, Notification> notification : this.notificationTable.entrySet()) {
            Audience audience = (Audience) notification.getValue().getAudience();
            if(audience.getTarget() == registerCode)
               result.add((Notification) notification);
        }

        return result;
    }

    @Override
    public long create(Notification notification) {
        this.notificationTable.put(notification.getId(), notification);
        return  notification.getId();
    }
}
