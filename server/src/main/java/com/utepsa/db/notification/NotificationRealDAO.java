package com.utepsa.db.notification;

import com.utepsa.api.notifications.Notification;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by shigeots on 03-11-16.
 */
public class NotificationRealDAO extends AbstractDAO<Notification> implements NotificationDAO {
    public NotificationRealDAO (SessionFactory factory) {
        super(factory);
    }

    @Override
    public Notification findById(long id) {
        return get(id);
    }

    @Override
    public List<Notification> findAll() {
        return list(namedQuery("com.utepsa.api.notifications.Notification.findAll"));
    }

    @Override
    public List<Notification> findByRegisterCode(String registerCode, int pageSize, int startRow) {
        return list(namedQuery("com.utepsa.api.notifications.Notification.findByRegisterCode").setParameter("registerCode",registerCode).setFirstResult(startRow).setMaxResults(pageSize));
    }

    @Override
    public long create(Notification notification) {
        final Notification created = persist(notification);
        if(created == null) try {
            throw new Exception("The notification was not created.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return created.getId();
    }
}
