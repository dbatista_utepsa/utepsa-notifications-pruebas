package com.utepsa.db.notification;

import com.utepsa.api.notifications.Notification;

import java.util.List;

/**
 * Created by shigeots on 03-11-16.
 */
public interface NotificationDAO {
    Notification findById (long id);

    List<Notification> findAll();

    List<Notification> findByRegisterCode (String registerCode, int pageSize, int startRow);

    long create (Notification notification);
}
