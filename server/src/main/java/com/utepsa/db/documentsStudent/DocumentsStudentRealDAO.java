package com.utepsa.db.documentsStudent;

import com.utepsa.api.documentsStudent.DocumentsStudent;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by shigeots on 31-10-16.
 */
public class DocumentsStudentRealDAO extends AbstractDAO<DocumentsStudent> implements DocumentsStudentDAO {

    public DocumentsStudentRealDAO (SessionFactory factory) {super (factory);}

    @Override
    public List<DocumentsStudent> getDocumentsStudentsByRegisterCode(String registerCode) {
        return list(namedQuery("com.utepsa.api.documentsStudent.DocumentsStudent.getDocumentsStudentsByRegisterCode").setParameter("registerCode",registerCode));
    }

    @Override
    public long create(DocumentsStudent documentsStudent) {
        final DocumentsStudent created = persist(documentsStudent);
        if (created == null) try {
            throw new Exception("The documentsStudent was not created.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  created.getId();
    }

    @Override
    public DocumentsStudent getDocumentsStudent(String documentCode) {
        return uniqueResult(namedQuery("com.utepsa.api.documentsStudent.DocumentsStudent.getDocumentStudentByDocumentCode").setParameter("documentCode",documentCode));
    }
}
