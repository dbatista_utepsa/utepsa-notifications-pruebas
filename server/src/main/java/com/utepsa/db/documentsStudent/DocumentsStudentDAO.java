package com.utepsa.db.documentsStudent;

import com.utepsa.api.documentsStudent.DocumentsStudent;

import java.util.List;

/**
 * Created by shigeots on 31-10-16.
 */
public interface DocumentsStudentDAO {

    List<DocumentsStudent> getDocumentsStudentsByRegisterCode (String registerCode);
    long create (DocumentsStudent documentsStudent);
    DocumentsStudent getDocumentsStudent(String documentCode);
}
