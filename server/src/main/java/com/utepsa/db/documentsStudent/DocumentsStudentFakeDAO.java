package com.utepsa.db.documentsStudent;

import com.utepsa.api.documentsStudent.DocumentsStudent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shigeots on 31-10-16.
 */
public class DocumentsStudentFakeDAO implements DocumentsStudentDAO {

    final private Map<Long, DocumentsStudent> documentsStudentTable = new HashMap<>();

    public DocumentsStudentFakeDAO() {
        DocumentsStudent documentsStudent = new DocumentsStudent(1, "0000376522", "001", "", "", "copia", "Fotocopia de carnet de identidad", "Falta");
        documentsStudentTable.put(documentsStudent.getId(), documentsStudent);
    }

    @Override
    public List<DocumentsStudent> getDocumentsStudentsByRegisterCode(String registerCode) {
        List<DocumentsStudent> documentsStudents = new ArrayList<>();
        for (Map.Entry<Long, DocumentsStudent> entry : documentsStudentTable.entrySet()) {
            if(entry.getValue().getRegisterCode().equals(registerCode))
                documentsStudents.add(entry.getValue());
        }
        return documentsStudents;
    }

    @Override
    public long create(DocumentsStudent documentsStudent) {
        this.documentsStudentTable.put(documentsStudent.getId(), documentsStudent);
        return documentsStudent.getId();
    }

    @Override
    public DocumentsStudent getDocumentsStudent(String documentCode) {
        for (Map.Entry<Long, DocumentsStudent> entry : documentsStudentTable.entrySet()) {
            if(entry.getValue().getDocumentCode().equals(documentCode))
                entry.getValue();
        }
        return null;
    }
}
