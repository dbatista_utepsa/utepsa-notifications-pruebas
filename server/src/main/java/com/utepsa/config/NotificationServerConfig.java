package com.utepsa.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class NotificationServerConfig extends Configuration {

    private String serverName;

    private Set<ExternalServer> externalServers;

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    public NotificationServerConfig() {}

    @JsonProperty
    public String getServerName() {
        return this.serverName;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return this.database;
    }

    @JsonProperty
    public void setDataSourceFactory(DataSourceFactory dataSourceFactory) {
        this.database = dataSourceFactory;
    }

    @JsonProperty("externalServers")
    public Set<ExternalServer> getExternalServers() {
        return externalServers;
    }

    @JsonProperty("externalServers")
    public void setExternalServers(Set<ExternalServer> externalServers) {
        this.externalServers = externalServers;
    }

    public ExternalServer getExternalSever(String name) {
        for (ExternalServer externalServer : this.externalServers) {
            if (externalServer.getName().equals(name))
                return externalServer;
        }
        return null;
    }
}
