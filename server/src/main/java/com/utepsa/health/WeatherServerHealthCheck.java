package com.utepsa.health;

import com.codahale.metrics.health.HealthCheck;
import com.utepsa.adapters.weather.WeatherAdapter;
import com.utepsa.config.ExternalServer;

/**
 * Created by roberto on 25/8/2016.
 */
public class WeatherServerHealthCheck extends HealthCheck {

    final private WeatherAdapter adapter;

    public WeatherServerHealthCheck(ExternalServer weatherServer) {
        this.adapter = new WeatherAdapter(weatherServer);
    }

    @Override
    protected Result check() throws Exception {
        if( this.adapter.isServerUp() ) {
            return Result.healthy("External Weather Service is Up and Ready");
        }
        return Result.healthy("External Weather Service is not running, this could affect the Notification Server");
    }
}
