package com.utepsa;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.api.auth.User;
import com.utepsa.api.courses.Course;
import com.utepsa.api.documentsStudent.DocumentsStudent;
import com.utepsa.api.historyNotes.HistoryNotes;
import com.utepsa.api.notifications.Notification;
import com.utepsa.api.students.Student;
import com.utepsa.config.ExternalServer;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.db.courses.CourseDAO;
import com.utepsa.db.courses.CourseRealDAO;
import com.utepsa.db.documentsStudent.DocumentsStudentDAO;
import com.utepsa.db.documentsStudent.DocumentsStudentRealDAO;
import com.utepsa.db.historyNotes.HistoryNotesDAO;
import com.utepsa.db.historyNotes.HistoryNotesRealDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.notification.NotificationRealDAO;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentRealDAO;
import com.utepsa.db.users.UserDAO;
import com.utepsa.db.users.UserRealDAO;
import com.utepsa.health.WeatherServerHealthCheck;
import com.utepsa.resources.adapted.utepsa.documentsStudent.DocumentsStudentAdapterResource;
import com.utepsa.resources.adapted.utepsa.documentsStudent.DocumentsStudentAdapterService;
import com.utepsa.resources.adapted.utepsa.historyNotes.HistoryNotesAdapterResource;
import com.utepsa.resources.adapted.utepsa.historyNotes.HistoryNotesAdapterService;
import com.utepsa.resources.adapted.utepsa.students.StudentsAdapterResource;
import com.utepsa.resources.adapted.utepsa.students.StudentsAdapterService;
import com.utepsa.resources.adapted.weather.WeatherResource;
import com.utepsa.resources.adapted.weather.WeatherService;
import com.utepsa.resources.auth.AuthResource;
import com.utepsa.resources.auth.AuthService;
import com.utepsa.resources.courses.CoursesResource;
import com.utepsa.resources.courses.CoursesService;
import com.utepsa.resources.documentsStudent.DocumentsStudentResource;
import com.utepsa.resources.documentsStudent.DocumentsStudentService;
import com.utepsa.resources.example.ExampleResource;
import com.utepsa.resources.example.ExampleService;
import com.utepsa.resources.notifications.NotificationService;
import com.utepsa.resources.notifications.NotificationsResource;
import com.utepsa.resources.students.StudentsResource;
import com.utepsa.resources.students.StudentsService;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class NotificationServerApp extends Application<NotificationServerConfig> {

    private final HibernateBundle<NotificationServerConfig> hibernate = new
            HibernateBundle<NotificationServerConfig>(Student.class, Notification.class, Course.class, HistoryNotes.class, User.class, DocumentsStudent.class)
    {
        @Override
        public DataSourceFactory getDataSourceFactory(NotificationServerConfig configuration) {

            return configuration.getDataSourceFactory();
        }
    };

    public static void main(String[] args) throws Exception {
        new NotificationServerApp().run(args);
    }

    @Override
    public String getName() {
        return "Notification Server";
    }

    @Override
    public void initialize(final Bootstrap<NotificationServerConfig> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(final NotificationServerConfig configuration,
                    final Environment environment){
        final StudentDAO studentDAO = new StudentRealDAO(hibernate.getSessionFactory());
        final NotificationDAO notificationDAO = new NotificationRealDAO(hibernate.getSessionFactory());
        final CourseDAO courseDAO= new CourseRealDAO(hibernate.getSessionFactory());
        final HistoryNotesDAO historyNotesDAO= new HistoryNotesRealDAO(hibernate.getSessionFactory());
        final UserDAO userDAO = new UserRealDAO(hibernate.getSessionFactory());
        final DocumentsStudentDAO documentsStudentDAO = new DocumentsStudentRealDAO(hibernate.getSessionFactory());
        this.setCORSconfiguration(environment);

        // Create services first
        ExampleService exampleService = new ExampleService(configuration.getServerName());
        ExternalServer gcmServer = configuration.getExternalSever("GCM");
        GCMAdapter gcmAdapter = new GCMAdapter(gcmServer);

        StudentsService studentsService = new StudentsService(studentDAO, historyNotesDAO);
        NotificationService notificationService = new NotificationService(notificationDAO, gcmAdapter, studentDAO);
        CoursesService coursesService = new CoursesService(courseDAO);
        AuthService authService = new AuthService(userDAO, studentDAO);
        DocumentsStudentService documentsStudentService = new DocumentsStudentService(documentsStudentDAO);

        // Register resources
        environment.jersey().register( new ExampleResource(exampleService));
        environment.jersey().register( new StudentsResource(studentsService, notificationService) );
        environment.jersey().register( new NotificationsResource(notificationService));
        environment.jersey().register( new CoursesResource(coursesService));
        environment.jersey().register( new ApiListingResource());
        environment.jersey().register( new AuthResource(authService));
        environment.jersey().register( new DocumentsStudentResource(documentsStudentService));

        // Register resources to use Adapters
        this.registerResourcesWithAdapters(environment, configuration);
        this.registerUtepsaResourcesWithAdapters(environment,configuration,studentDAO,historyNotesDAO, userDAO, documentsStudentDAO);

        //initializing apiListingResource
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        BeanConfig config = new BeanConfig();
        config.setTitle("UTEPSA Notifications API");
        config.setVersion("1.0.0");
        config.setBasePath("/api");
        config.setResourcePackage("com.utepsa");
        config.setScan(true);

    }

    private void registerResourcesWithAdapters(Environment environment, NotificationServerConfig config) {
        final ExternalServer weatherServer = config.getExternalSever("Weather");
        final WeatherService service = new WeatherService( weatherServer );

        environment.jersey().register( new WeatherResource(service) );
        environment.healthChecks().register("Weather", new WeatherServerHealthCheck(weatherServer));
    }

    private void registerUtepsaResourcesWithAdapters(Environment environment, NotificationServerConfig config, StudentDAO studentDao, HistoryNotesDAO historyNotesDAO, UserDAO userDAO, DocumentsStudentDAO documentsStudentDAO){
        final ExternalServer utepsaServer = config.getExternalSever("Utepsa");
        final StudentsAdapterService utepsaStudentService = new StudentsAdapterService(utepsaServer, studentDao, userDAO);
        final HistoryNotesAdapterService utepsaHistoryNotesService = new HistoryNotesAdapterService(utepsaServer,historyNotesDAO,studentDao);
        final DocumentsStudentAdapterService documentsStudentAdapterService = new DocumentsStudentAdapterService(utepsaServer, documentsStudentDAO, studentDao);
        environment.jersey().register( new StudentsAdapterResource(utepsaStudentService));
        environment.jersey().register( new HistoryNotesAdapterResource(utepsaHistoryNotesService));
        environment.jersey().register( new DocumentsStudentAdapterResource(documentsStudentAdapterService));
    }

    private void setCORSconfiguration(Environment environment) {
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,accountOrRegisterCode,password");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,accountOrRegisterCode,password");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
    }
}
