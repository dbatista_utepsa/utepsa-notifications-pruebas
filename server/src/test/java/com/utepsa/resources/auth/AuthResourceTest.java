package com.utepsa.resources.auth;

import com.utepsa.api.students.Student;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentFakeDAO;
import com.utepsa.db.users.UserDAO;
import com.utepsa.db.users.UserFakeDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by david on 19/10/16.
 */
public class AuthResourceTest {
    final private static UserDAO userDAO = new UserFakeDAO();
    final private static StudentDAO studentDAO = new StudentFakeDAO();
    final private static AuthService authService = new AuthService(userDAO,studentDAO);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new AuthResource(authService))
            .build();

    @Test
    public void testGetCourseWithExistingCode() throws Exception {
        Response response = resources.client()
                .target("/auth").request()
                .header("accountOrRegisterCode","403202")
                .header("password","123456").post(Entity.text(""));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }
}
