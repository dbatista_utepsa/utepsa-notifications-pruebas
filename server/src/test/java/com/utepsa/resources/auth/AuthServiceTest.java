package com.utepsa.resources.auth;

import com.utepsa.api.students.Student;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentFakeDAO;
import com.utepsa.db.users.UserDAO;
import com.utepsa.db.users.UserFakeDAO;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * Created by david on 19/10/16.
 */
public class AuthServiceTest {
    private UserDAO userDAO;
    private StudentDAO studentDAO;
    private AuthService authService;

    @Before
    public void setup() {
        this.userDAO = new UserFakeDAO();
        this.studentDAO = new StudentFakeDAO();
        this.authService = new AuthService(userDAO, studentDAO);
    }

    @Test
    public void testLoginCorrectUser() throws Exception {
        Student student =  authService.login("403202","123456");
        assertThat(student.getRegisterCode()).isEqualTo("403202");
    }

    @Test
    public void testLoginInorrectUser() throws Exception {
        assertThatExceptionOfType(Throwable.class).isThrownBy(() -> authService.login("403201","123456")).withMessage("User does not exist");
    }
}
