package com.utepsa.resources.courses;

import com.utepsa.api.courses.Course;
import com.utepsa.db.courses.CourseDAO;
import com.utepsa.db.courses.CourseFakeDAO;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Roberto Perez on 10-08-16.
 */
public class CourseServiceTest {

    private CourseDAO dao;
    private CoursesService coursesService;

    @Before
    public void setup() {
        this.dao = new CourseFakeDAO();
        this.coursesService = new CoursesService(dao);
    }

    @Test
    public void testGetExistingCourse() throws Exception {
        Course course = new Course(1010, "SIS-100", "Programacion I", "123-ABC");
        this.dao.create(course);
        assertThat(this.coursesService.getByCode(1010)).isEqualTo(course);
    }

    @Test(expected = NotFoundException.class)
    public void testGetNonExistingCourse() throws NotFoundException {
        this.coursesService.getByCode(58);
    }

    @Test()
    public void testGetEmptyCoursesList(){
        assertThat(this.coursesService.getAllCourses().isEmpty()).isTrue();
    }

    @Test()
    public void testGetCoursesList(){
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1010, "SIS-100", "Course 1", "123-ABC"));
        courses.add(new Course(2020, "SIS-200", "Course 2", "123-ABC"));
        courses.add(new Course(3030, "SIS-300", "Course 3", "123-ABC"));
        courses.add(new Course(4040, "SIS-400", "Course 4", "123-SBC"));
        courses.add(new Course(5050, "SIS-500", "Course 5", "123-ABC"));

        courses.forEach(course -> this.dao.create(course));

        assertThat(this.coursesService.getAllCourses()).isEqualTo(courses);
    }
}
