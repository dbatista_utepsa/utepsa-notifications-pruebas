package com.utepsa.resources.courses;

import com.utepsa.api.courses.Course;
import com.utepsa.db.courses.CourseDAO;
import com.utepsa.db.courses.CourseFakeDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Roberto Perez on 01-08-16.
 */
public class CourseResourceTest {

    final private static CourseDAO dao = new CourseFakeDAO();
    final private static CoursesService coursesService = new CoursesService(dao);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new CoursesResource(coursesService))
            .build();

    private Course course;
    private List<Course> courses;

    @Before
    public void setUp() {
        this.course = new Course(1010, "SIS-1010", "Course 1", "123-ABC");
        dao.create(this.course);

        this.courses = new ArrayList<>();
        courses.add(this.course);
        courses.add(new Course(2020, "SIS-200", "Course 2", "123-ABC"));
        courses.add(new Course(3030, "SIS-300", "Course 3", "123-ABC"));
        courses.add(new Course(4040, "SIS-400", "Course 4", "123-SBC"));
        courses.add(new Course(5050, "SIS-500", "Course 5", "123-ABC"));

        courses.forEach(c -> this.dao.create(c));
    }

    @Test
    public void testGetCourseWithExistingCode() throws Exception {
        Course request = resources.client().target("/courses/1010").request().get(Course.class);
        assertThat(request).isEqualTo(course);
    }

    @Test(expected = NotFoundException.class)
    public void testGetCourseWithNonExistingCode() throws Exception {
        Course request = resources.client().target("/courses/99965").request().get(Course.class);
        assertThat(request).isNull();
    }

    @Test()
    public void testGetAllCourses() throws Exception {
        List<Course> response = resources.client().target("/courses")
                .request().get(new GenericType<List<Course>>(){});
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(this.courses);
    }
}
