package com.utepsa.resources.students;

import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.api.students.Student;
import com.utepsa.db.historyNotes.HistoryNotesDAO;
import com.utepsa.db.historyNotes.HistoryNotesFakeDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentFakeDAO;
import com.utepsa.resources.notifications.NotificationService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class StudentsResourceTest {

    final private static StudentDAO dao = new StudentFakeDAO();
    final private static HistoryNotesDAO historyNotesDAO = new HistoryNotesFakeDAO();
    final private static StudentsService studentsService = new StudentsService(dao, historyNotesDAO);
    private static NotificationDAO daoNotification = Mockito.mock(NotificationDAO.class);
    private static GCMAdapter adapter = Mockito.mock(GCMAdapter.class);
    private static NotificationService notificationService = new NotificationService(daoNotification, adapter, dao);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new StudentsResource(studentsService, notificationService))
            .build();

    private Student student;
    private List<Student> students;

    @Before
    public void setup() {
        this.students = new ArrayList<>();
        Student studentL = new Student(1,"0000376520", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376520", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
        this.student = studentL;
        this.students.add(studentL);
    }

    @Test
    public void testGetAllStudents () throws  Exception {
        Student studentL;
        studentL = new Student(2,"0000376521", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376521", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
        this.dao.create(studentL);
        this.students.add(studentL);

        studentL = new Student(3,"0000376522", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376522", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
        this.dao.create(studentL);
        this.students.add(studentL);

        List<Student> response = resources.client().target("/students")
                .request().get(new GenericType<List<Student>>(){});
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(this.students);
    }

    @Test
    public void testGetStudentWithValidCiOrRegisterCode() throws  Exception {
        Response response = resources.client()
                .target("/students/0000376520").request()
                .get(Response.class);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public void testCreateAstudentWithPOST() throws Exception {
        Response response = resources.client()
                .target("/students").request()
                .post(Entity.json(student));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);
    }

    @Test
    public void testGetStudentWithInvalidCiOrRegisterCode() throws  Exception {
        Response response = resources.client()
                .target("/students/3765").request()
                .get(Response.class);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }
}
