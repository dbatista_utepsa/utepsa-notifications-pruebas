package com.utepsa.resources.students;

import com.utepsa.api.students.Student;
import com.utepsa.db.historyNotes.HistoryNotesDAO;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentFakeDAO;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class StudentsServiceTest {

    private StudentDAO dao;
    private HistoryNotesDAO historyNotesDAO;
    private StudentsService studentsService;
    private Student student;
    private Student newStudent;

    @Before
    public void setup() {
        this.student = new Student(1,"0000376520", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376520", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
        this.dao = new StudentFakeDAO();
        this.dao.create(this.student);
        this.studentsService = new StudentsService(dao, historyNotesDAO);
    }

    @Test
    public void testCreateAValidStudent() throws Exception {
        this.newStudent = new Student(2,"0000376521", "Fernando", "Flores", "Parada", "26/01/1992", "CI", "6345870","77055560", "3376150", "101", "0000376521", "fflores", "12345678", "fflores.est.@utepsa.edu", "");
        assertThat(studentsService.createStudent(newStudent)).isNotNull();
    }

    @Test
    public  void testGetStudentWithValidCiOrRegisterCode() throws Exception {
        Student student = new Student(1,"0000376520", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376520", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
        assertThat(this.studentsService.getStudentByDocumentCodeOrRegisterCode("0000376520")).isEqualTo(this.student);
    }

    @Test (expected = WebApplicationException.class)
    public  void testGetStudentWithInvalidCiOrRegisterCode() throws Exception {
        assertThat(this.studentsService.getStudentByDocumentCodeOrRegisterCode("3765")).isNull();
    }
}
