package com.utepsa.resources.students;

import com.utepsa.api.students.Student;
import com.utepsa.resources.notifications.NotificationService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class StudentsResourceGrizzlyTest {

    private static final StudentsService serviceMock = Mockito.mock(StudentsService.class);
    private static final NotificationService notificationServiceMock = Mockito.mock(NotificationService.class);
    @ClassRule
    public static final ResourceTestRule RULE = ResourceTestRule.builder()
            .addResource(new StudentsResource(serviceMock, notificationServiceMock))
            .setTestContainerFactory(new GrizzlyWebTestContainerFactory())
            .build();

    @Test
    public void testGetStudentByIdResource() {
        Student student = new Student(1,"0000376520", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376520", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");

        Mockito.when(serviceMock.getStudentByDocumentCodeOrRegisterCode("0000376520")).thenReturn(student);

        Student found = RULE.getJerseyTest().target("/students/0000376520")
                .request().get(Student.class);

        assertThat(found).isEqualTo(student);
    }
}
