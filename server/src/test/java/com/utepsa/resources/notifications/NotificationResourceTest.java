package com.utepsa.resources.notifications;

import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.api.notifications.Notification;
import com.utepsa.api.notifications.audience.Audience;
import com.utepsa.api.notifications.audience.AudienceType;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.notification.NotificationFakeDAO;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentFakeDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.apache.http.HttpStatus;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by DAvid on 22/07/2016.
 */
public class NotificationResourceTest {

    final private static NotificationDAO dao = new NotificationFakeDAO();
    final private static StudentDAO studentDAO = new StudentFakeDAO();
    private static GCMAdapter adapter = Mockito.mock(GCMAdapter.class);
    private static final NotificationService notificationService = new NotificationService(dao, adapter, studentDAO);

    @ClassRule
    public static final ResourceTestRule resouces = ResourceTestRule.builder()
            .addResource(new NotificationsResource(notificationService))
            .build();


    private Notification notification;
    private List<Notification> notifications;

    @Before
    public void setup() {
        Set<Audience> audience = Collections.newSetFromMap(new IdentityHashMap<Audience,Boolean>());
        audience.add(new Audience("public", AudienceType.PUBLIC));

        this.notifications = new ArrayList<>();
        this.notification = new Notification(audience,1469185500, "Apertura de SW Factory", "Les invitamos a...", "Notice");
        notification.setId(1);
        dao.create(this.notification);
        notifications.add(notification);
        notifications.forEach(c -> this.dao.create(c));
    }

    @Test
    public void testGetValidStudentByIdResource() {
        Notification request = resouces.client().target("/notifications/1").request().get(Notification.class);
        assertThat(request).isEqualTo(notification);
    }

    @Test
    public void testGetNotificationWithNonExtingId() throws Exception {
        Notification request = resouces.client().target("/notifications/99999999999").request().get(Notification.class);
        assertThat(request).isNull();
        assertThat(request).isNotEqualTo(notification);
    }

    @Test
    public void testGetAllNotifications() throws Exception {
        List<Notification> response = resouces.client().target("/notifications").request().get(new GenericType<List<Notification>>(){});
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(this.notifications);
    }

/*  Test de post pendiente
    @Test
    public void testCreateAnotificationWithPOST() throws Exception {
        Response response = resouces.client().target("/notifications").request().post(Entity.json(notification));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);
    }
    */
}
