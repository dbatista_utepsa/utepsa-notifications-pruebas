package com.utepsa.resources.notifications;

import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.api.notifications.Notification;
import com.utepsa.api.notifications.audience.Audience;
import com.utepsa.api.notifications.audience.AudienceType;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.notification.NotificationFakeDAO;
import com.utepsa.db.students.StudentDAO;
import com.utepsa.db.students.StudentFakeDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.reset;

/**
 * Created by DAvid on 22/07/2016.
 */
public class NotificationServiceTest {
    private static NotificationDAO dao;
    private static StudentDAO studentDAO;
    private static GCMAdapter adapter = Mockito.mock(GCMAdapter.class);
    private static NotificationService notificationService = new NotificationService(dao, adapter,studentDAO);

    private Notification notification;
    private List<Notification> notifications;

    @Before
    public void setup() {
        this.dao = new NotificationFakeDAO();
        this.studentDAO = new StudentFakeDAO();
        this.notificationService = new NotificationService(dao, adapter,studentDAO);

        Set<Audience> audience = Collections.newSetFromMap(new IdentityHashMap<Audience,Boolean>());
        audience.add(new Audience("public", AudienceType.PUBLIC));
        this.notifications = new ArrayList<>();
        this.notification = new Notification(audience,1469185500, "Apertura de SW Factory", "Les invitamos a...", "Notice");
        notification.setId(1);
        dao.create(this.notification);
        notifications.add(notification);
        notifications.forEach(c -> this.dao.create(c));
    }

    @Test
    public void testGetNotificationWithValidId() throws Exception {
        assertThat(notificationService.getNotificationById(1)).isEqualTo(notification).isNotNull();
    }

    @Test
    public void testGetNotificationWithInvalidId() throws Exception {
        assertThat(notificationService.getNotificationById(999999999)).isNull();
    }

    @Test
    public void testGetListNotifications() throws Exception {
        assertThat(notificationService.getAllNotifications()).isEqualTo(notifications).isNotNull();
    }
}
