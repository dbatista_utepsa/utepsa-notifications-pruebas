package com.utepsa.api.documentsStudent;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by shigeots on 31-10-16.
 */
public class DocumentsStudentTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private  DocumentsStudent documentsStudent;

    @Before
    public void setUp() throws  Exception {
        this.documentsStudent = new DocumentsStudent(1, "0000376522", "001", "", "", "copia", "Fotocopia de carnet de identidad", "Falta");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/documentsStudent.json"), DocumentsStudent.class));

        assertThat(MAPPER.writeValueAsString(documentsStudent)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/documentsStudent.json"),
                DocumentsStudent.class)).isEqualTo(documentsStudent);
    }
}
