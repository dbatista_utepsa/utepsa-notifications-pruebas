package com.utepsa.api.notifications;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.api.notifications.audience.Audience;
import com.utepsa.api.notifications.audience.AudienceType;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 19/7/2016.
 */
public class NotificationTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Notification notification ;

    @Before
    public void setup() {
        Set<Audience> audience = Collections.newSetFromMap(new IdentityHashMap<Audience,Boolean>());
        audience.add(new Audience("public", AudienceType.PUBLIC));

        this.notification = new Notification(audience,1469185500, "Apertura de SW Factory", "Les invitamos a...", "Notice");
        notification.setId(1);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/notification.json"), Notification.class));
        assertThat(MAPPER.writeValueAsString(notification)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/notification.json"),
                Notification.class)).isEqualTo(notification);
    }
}