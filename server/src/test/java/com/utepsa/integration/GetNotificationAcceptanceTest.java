package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.api.notifications.Notification;
import com.utepsa.api.notifications.audience.Audience;
import com.utepsa.api.notifications.audience.AudienceType;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.util.Duration;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by David on 26/07/2016.
 */
public class GetNotificationAcceptanceTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Test
    public void requestStudentsNotificacionsByRegisterCode() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test valid Register Code in Student Notification");

        Response response = client.target(
                String.format("http://localhost:%d/api/students/403201/notifications/0", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public void testCreateAnotificationsWithPOST() throws Exception {
        Set<Audience> audience = Collections.newSetFromMap(new IdentityHashMap<Audience,Boolean>());
        audience.add(new Audience("Public", AudienceType.PUBLIC));

        Notification notification = new Notification(audience,1469185500,"Titulo del test","Esto es un test de prueba","TEST");
        Entity n = Entity.json(notification);

        JerseyClientConfiguration config = new JerseyClientConfiguration();
        config.setTimeout(Duration.seconds(20));

        Client client = new JerseyClientBuilder(RULE.getEnvironment()).using(config).build("test create notification");


        Response response = client
                .target(String.format("http://localhost:%d/api/notifications", RULE.getLocalPort()))
                .request()
                .post(Entity.json(notification));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);
    }
}
