package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.config.NotificationServerConfig;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 10-08-16.
 */
public class GetCourseAcceptanceTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);


    @Test
    public void requestCourseWithExistingCode() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test GET Existing Course");

        Response response = client.target(
                String.format("http://localhost:%d/api/courses/581", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public void requestCourseWithNonExistingCode() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test GET Non Existing Course");

        Response response = client.target(
                String.format("http://localhost:%s/api/courses/0", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void requestGetAllCourses() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test GET All Courses");

        Response response = client.target(
                String.format("http://localhost:%s/api/courses", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }
}
