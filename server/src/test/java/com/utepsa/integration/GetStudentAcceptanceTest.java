package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.api.students.Student;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class GetStudentAcceptanceTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Test
    public void testCreateAstudentWithPOST() throws Exception {
        Student student = new Student(1,"0000376520", "Armando", "Carpa", "Roja", "26/01/1994", "CI", "6345870","77055560", "3376150", "101", "0000376520", "acarpa", "12345678", "acarpa.est.@utepsa.edu", "");
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("test client");

        Response response = client
                .target(String.format("http://localhost:%d/api/students", RULE.getLocalPort()))
                .request()
                .post(Entity.json(student));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_CREATED);
    }

    @Test
    public  void requestAstudentWithValidCiOrRegisterCodeGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test valid CI or Register Code");

        Response response = client.target(
                String.format("http://localhost:%d/api/students/000046885A", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_OK);
    }

    @Test
    public  void requestAstudentWithInvalidCiOrRegisterCodeGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test invalid CI or Register Code");

        Response response = client.target(
                String.format("http://localhost:%d/api/students/37", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.SC_UNPROCESSABLE_ENTITY);
    }
}
