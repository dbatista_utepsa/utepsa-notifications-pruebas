package com.utepsa.adapters.gcm;

import com.utepsa.config.ExternalServer;
import org.apache.http.HttpStatus;
import org.glassfish.grizzly.utils.ArraySet;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by roberto on 25/8/2016.
 */
public class GCMAdapterTest {

    private GCMAdapter adapter;
    private Set<String> ids;

    @Before
    public void SetUp() {
        ExternalServer server = new ExternalServer();
        server.setName("GCM");
        server.setUrl("https://gcm-http.googleapis.com/gcm/send");
        server.setKey("AIzaSyD7BSAKmZNwG2DQILjX1aPg8yCK_ciPgzs");

        // Just for test using Alexis DeviceID
        String[] idsList = {
                "d_O2BbRSWKc:APA91bEN15SOHfCsVvP_PoQlyYN3f2np2NLIss4WcTct2D-LOdCJAz5NpJhqf9ENt52o1e5-mjNbuGz6dkKKR2GZxyY3xviQGMvP54KyddMh82hhtHzK7RMcTT_wNwWVh27t8mTplZkM"   };

        this.ids = new HashSet<>(Arrays.asList(idsList));

        this.adapter = new GCMAdapter(server);
    }

    @Test
    public void sendSimpleNotification() throws Exception {
        NotificationGCM notification = new NotificationGCM();
        notification.setData(new Data("Test", "This is a test notification"));
        notification.setRegistration_ids(this.ids);

        assertEquals(HttpStatus.SC_OK, this.adapter.sendNotification(notification));
    }
}
