package com.utepsa.adapters.utepsa.documentsStudent;

import com.utepsa.config.ExternalServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by david on 28/10/16.
 */
public class DocumentsStudentAdapterTest {
    private DocumentsStudentAdapter adapter;

    @Before
    public void SetUp() {
        ExternalServer externalServer = new ExternalServer();
        externalServer.setName("Utepsa");
        externalServer.setUrl("http://190.171.202.86/api/");

        this.adapter = new DocumentsStudentAdapter(externalServer);
    }

    @Test
    public void testIsExternalServerUP() throws IOException {
        Assert.assertTrue(this.adapter.isServerUp());
    }

    @Test
    public void testGetAllHistoryNotesForStudent() throws IOException {
        List<DocumentsStudentData> listDocumentsStudent = this.adapter.getAllDocumentsStudentForStudent("0000000018");

        Assert.assertNotNull(listDocumentsStudent);
    }
}
