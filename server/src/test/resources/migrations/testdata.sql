--insert data student
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('0000376522','Shigeo','Tsukazan','Shiroma','26/01/1993','CI','6345872',77055568,3376153,'101','0000376522','shigeots','12345678','stsukazan.est.@utepsa.edu','');
INSERT INTO Student (agendaCode, name, fatherLastName, motherLastName, birthdate, documentType, documentCode, phoneNumber1, phoneNumber2, careerCode,registerCode, userAccount, password, email, gcmId) VALUES ('000046885A', 'Geraldo', 'Figueroa','Zurita','19/02/1997','CI','9746660',78590523,3536889,'101','000046885A','geraldofz','12345678','gfigueroa.est@utepsa.edu', '');

--insert data notification
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Primera noticia', 'Esta es la primera noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Segunda noticia', 'Esta es la segunda noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Tercera noticia', 'Esta es la tercera noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Cuarta noticia', 'Esta es la Cuarta noticia del sistema.', 'NOTICE');
INSERT INTO Notification (creationTimestamp, title, content, type) VALUES (1469185500, 'Quinta noticia', 'Esta es la Quinta noticia del sistema.', 'NOTICE');

--insert data audience
INSERT INTO Audience (notification_id, target, type) VALUES (1,'PUBLIC', 'PUBLIC');
INSERT INTO Audience (notification_id, target, type) VALUES (2,'PUBLIC', 'PUBLIC');
INSERT INTO Audience (notification_id, target, type) VALUES (3,'374201', 'STUDENT');
INSERT INTO Audience (notification_id, target, type) VALUES (4,'123123', 'STUDENT');
INSERT INTO Audience (notification_id, target, type) VALUES (5,'374201', 'STUDENT');

--insert data courses
INSERT INTO Course (code, initials, name, careerCode) VALUES(579,'COM-100','ORGANIZACION PERSONAL (FET)','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(580,'EXT-100','ELEMENTOS DE ARITMETICA','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(581,'COM-110','INGLES I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(582,'SIS-100','DESARROLLO DE APLICACIONES PARA INTERNET I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1089,'SIP-100','INTRODUCCION A LA INFORMATICA','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(584,'SIS-110','INTRODUCCION A LA PROGRAMACION','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(585,'EXT-110','ELEMENTOS DE ALGEBRA','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(586,'COM-120','INGLES II','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1090,'SIP-110','INTRODUCCION A REDES','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(591,'EMP-100','ADMINISTRACION','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(809,'EXT-130','MATEMATICAS I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(823,'SIS-120','PROGRAMACION I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(973,'SIS-170','SISTEMAS OPERATIVOS','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1093,'SIP-200','ADMINISTRACION DE SISTEMAS OPERATIVOS I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1091,'SIP-210','ADMINISTRACION DE SISTEMAS OPERATIVOS II','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(594,'EMP-140','SISTEMAS ORGANIZACIONALES','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(971,'EXT-150','MATEMATICAS II ADMINISTRATIVA','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(972,'SIS-130','PROGRAMACION II','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1092,'SIP-220','ADMINISTRACION DE SISTEMAS OPERATIVOS III','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1094,'SIP-230','ADMINISTRACION DE SISTEMAS OPERATIVOS IV','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(881,'SIS-150','BASE DE DATOS I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(969,'EMP-160','CONTABILIDAD','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1007,'SIS-140','PROGRAMACION III','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(893,'EXT-180','PROBABILIDAD Y ESTADISTICA','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1095,'SIP-300','DESARROLLO DE APLICACIONES I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1096,'SIP-310','ADMINISTRACION DE BASES DE DATOS','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1098,'SIP-320','PROGRAMACION DE BASES DE DATOS','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(597,'COM-130','MEDIO AMBIENTE','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1025,'SIS-160','ANALISIS DE SISTEMAS I','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1099,'SIP-330','DESARROLLO DE APLICACIONES II','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1100,'SIP-340','DESARROLLO DE APLICACIONES III','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(20504,'COM-180','ELECTIVA II(EMP I)','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1101,'SIP-350','PROYECTO INTEGRAL DE SISTEMAS','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(979,'COM-140','ANALISIS DE ENTORNO','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1102,'SIP-400','DESARROLLO DE APLICACIONES IV','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1103,'SIP-410',,'DESARROLLO DE APLICACIONES V','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(20510,'COM-190','ELECTIVA III(EMP II)','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1104,'SIP-420','ADMINISTRACION DE SISTEMAS OPERATIVOS V','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1105,'SIP-430','ADMINISTRACION DE SISTEMAS OPERATIVOS VI','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(901,'SIS-230','ANALISIS DE SISTEMAS II','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(908,'SIS-250','INGENIERIA DE SOFTWARE','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(899,'SIS-210','COMERCIO ELECTRONICO','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(1180,'COM-170','ELECTIVA I (SISTEMAS)','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(992,'EMP-120','PLANEACION Y PROYECTOS','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(897,'EMP-110','LEGISLACION PARA INGENIEROS','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(20486,'SIP-450','PRACTICAS PROFESIONALES','123-ABC');
INSERT INTO Course (code, initials, name, careerCode) VALUES(20498,'COM-200','PROYECTO EMPRESARIAL','123-ABC');
