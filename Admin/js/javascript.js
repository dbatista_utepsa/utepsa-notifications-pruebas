var hourToTimestamp;

function sendData(){
    var register = $("#register").val();
    if(register == ""){
      register = "PUBLIC";
    }

    var formData = {
                      "id": 0,
                      "audience": [
                        {
                          "target": register,
                          "type": type
                        }
                      ],
                      "creationTimestamp": hourToTimestamp,
                      "title": $("#title").val(),
                      "content": $("#contentArea").val(),
                      "type": $("#optionNotice").val()
                    }
 
    $.ajax({
         url : "http://52.43.108.208:9090/api/notifications",
         type: 'post', 
         data : JSON.stringify(formData),
         contentType: 'application/json',
         dataType: 'json',
         statusCode: {
           201: function (response){
            alert("notificacion enviada a los estudiantes");
           },
           406: function (response){
            alert("notificacion no enviada a los estudiantes");
           }
        }
    });
}

function showContent(){
    element = document.getElementById("content");
    check = document.getElementById("publicCheck");
    check = document.getElementById("studentCheck");

    if(check.checked)
        element.style.display='block';
    else {
        element.style.display = 'none';
    }
}

var type;
function typeNotice(){
  check1 = document.getElementById("publicCheck");
  check1 = document.getElementById("studentCheck");

  if(check1.checked)
    type="STUDENT";
  else {
    type="PUBLIC";
    }
}

function cleanTextBox(){
    document.getElementById("contentArea").value = null;
    document.getElementById("title").value = null;
    document.getElementById("register").value = null;
}
 
  var dateFull = new Date();
  var day = dateFull.getDate();
  var month = dateFull.getMonth()+1;
  var year = dateFull.getFullYear();
  var hour = dateFull.getHours();
  var minutes = dateFull.getMinutes();
  var seconds = dateFull.getSeconds();

function humanToTime(){
    var humDate = new Date(Date.UTC(year,
          (month),
          (day),
          (hour),
          (minutes),
          (seconds)));
    hourToTimestamp = (humDate.getTime()/1000.0);
}