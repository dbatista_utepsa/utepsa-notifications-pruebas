;(function() {

    angular.module("utepsa-notifications").factory("documentsStudentService", documentsStudentService);

    documentsStudentService.$inject = ['$http','API','$localStorage'];

    function documentsStudentService($http,API,$localStorage) {

        function getDocumentsStudent(){
            return $http({
                method: 'GET',
                url: API.url+'documentsStudent/'+ $localStorage.currentUser.registerCode,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }).then(function(itemshistoryNotes) {
                return itemshistoryNotes.data;
            })
                .catch(function(error) {
                    return error;
                });
        }

        var service={
            getDocumentsStudent: getDocumentsStudent,
        };
        return service;
    }

})();
