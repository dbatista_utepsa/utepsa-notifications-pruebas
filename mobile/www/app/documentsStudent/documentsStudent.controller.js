;(function() {

    angular.module("utepsa-notifications").controller('DocumentsStudentController', DocumentsStudentController);

    DocumentsStudentController.$inject = ['$scope', 'resizeTemplate','documentsStudentService'];

    function DocumentsStudentController($scope, resizeTemplate, documentsStudentService) {

        $scope.documentsStudentItems = null;
        resizeTemplate.resize();
        getDocumentsStudent();
        $scope.spinnerDocumentsStudent = true;

        function getDocumentsStudent(){
            var promise = documentsStudentService.getDocumentsStudent();
            if(promise){
                promise.then(function (result) {
                    $scope.documentsStudentItems = result;
                    $scope.spinnerDocumentsStudent = false;
                });
            }
        }

        $scope.numberDocuments = function(){
            var total = 0;
            angular.forEach($scope.documentsStudentItems, function(item){
                if(item.state == "ENTREGADO"){
                    total ++;
                }
            });
            return total;
        };
    }

})();