;(function() {

	angular.module("utepsa-notifications").factory("notificationsService", notificationsService);

	notificationsService.$inject = ['$http', 'API','$localStorage'];
	
	function notificationsService($http, API,$localStorage){

		function getNotifications(rows){
			if (!$localStorage.currentUser) {
				return false;
			}

			return $http({
			  method: 'GET',
			  url: API.url + 'students/' + $localStorage.currentUser.registerCode +'/notifications/' + rows, 
       		  withCredentials: true,
	          headers: {
	          	'Content-Type': 'application/json; charset=utf-8'
	          }
			}).then(function(notifications) {
			  return notifications.data;
			})
			.catch(function(error) {
			  return error;
			});
		}

		var service = {
			getNotifications: getNotifications
		};

		return service;
	}

})();