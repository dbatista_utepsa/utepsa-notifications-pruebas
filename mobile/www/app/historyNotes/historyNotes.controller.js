;(function() {

  angular.module("utepsa-notifications").controller('HistoryNotesController', HistoryNotesController);

  HistoryNotesController.$inject = ['$scope', 'resizeTemplate', 'historyNotesService'];

  function HistoryNotesController($scope, resizeTemplate, historyNotesService) {
    $scope.itemshistoryNotes=[];
    resizeTemplate.resize();
    getHistoryNote();
    $scope.spinnerHistoryNotes = true;

    function getHistoryNote(){
      var promise = historyNotesService.getHistoryNotes();
      if(promise){
        promise.then(function (result) {
          $scope.itemshistoryNotes = result;
          $scope.spinnerHistoryNotes = false;
        });
      }
    }

    $scope.promNote = function(){
      var total = 0;
      var finalnote = 0;
      var count = 0;
      angular.forEach($scope.itemshistoryNotes, function(item){
        total += parseInt(item.note);
        count++;
      });
      finalnote =parseInt(total/$scope.itemshistoryNotes.length);
      return finalnote;
    };
  }
})();