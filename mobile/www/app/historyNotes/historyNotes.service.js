;(function() {

	angular.module("utepsa-notifications").factory("historyNotesService", historyNotesService);

	historyNotesService.$inject = ['$http','API','$localStorage'];

	function historyNotesService($http,API,$localStorage) {

		function getHistoryNotes(){
			return $http({
				method: 'GET',
				url: API.url+'students/'+ $localStorage.currentUser.registerCode+'/historyNotes',
				withCredentials: true,
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			}).then(function(itemshistoryNotes) {
				return itemshistoryNotes.data;
			})
				.catch(function(error) {
					return error;
				});
		}

		var service={
			getHistoryNotes: getHistoryNotes,
		};
		return service;
	}

})();