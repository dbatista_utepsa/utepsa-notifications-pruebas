;(function() {

  	angular.module("utepsa-notifications").controller('LoginController', LoginController);

  	LoginController.$inject = ['$scope', '$state', 'resizeTemplate', 'AuthenticationService', 'API'];

  	function LoginController($scope, $state, resizeTemplate, AuthenticationService, API) {
    	resizeTemplate.resize();
    	var vm = this;
        vm.login = login;

        init();

        function init() {
            // reset login status
            //AuthenticationService.Logout();
        };

        // check is number
        var registerCodeIsNumber = function (accountOrRegisterCode) {
            var reg = new RegExp("^[-]?[0-9]+[\.]?[0-9]+$");
            return reg.test(accountOrRegisterCode);
        }
        
        var formatRegisterCode = function(accountOrRegisterCode, tamano) {

            if(registerCodeIsNumber(accountOrRegisterCode)) {
                return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
            }else {
                return accountOrRegisterCode;
            }
        }
        function login() {
            vm.loading = true;
            var user =formatRegisterCode(vm.username,10);
            AuthenticationService.Login(user, vm.password, function (result) {
                if (result === true) {
                    registrarGCM(user);
                    $state.go('notifications');
                } else {
                    vm.error = 'Username or password is incorrect';
                    vm.loading = false;
                }
            });
    	};

        function reloadPage(){ 
            $state.reload();
        } 

        function registrarGCM(user){
            vm.loading = true;
            var options = {};
            options.android = {};
            options.android.senderID = API.sender_id;

            var push = PushNotification.init(options);

            push.on('registration', function(data) {
                AuthenticationService.postGcmRegistration(user, data.registrationId, function (result) {
                    if (result === true) {
                        //console.log('GCM registration complete. Id is: ', data.registrationId);
                    } else {
                        //console.log('No se pudo registrar el dispositivo');
                         vm.loading = false;
                    }
                 });
            });

            push.on('notification', function(data) {
                //console.log('Notification recieved: ', data);
                navigator.notification.alert( 
                    data.message,         // message 
                    reloadPage,       // callback 
                    data.title,           // title 
                    'Ok'                  // buttonName 
                ); 
            });

            push.on('error', function(e) { 
            });
         }
  	}	

})();