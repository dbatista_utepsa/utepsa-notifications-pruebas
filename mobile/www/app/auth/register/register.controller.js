;(function() {

  angular.module("utepsa-notifications").controller('RegisterController', RegisterController);

  RegisterController.$inject = ['$scope', 'resizeTemplate','registerService','$state'];

  function RegisterController($scope, resizeTemplate,registerService,$state) {
    resizeTemplate.resize();
    var vm = this;
    vm.postCreateStudent = postCreateStudent;

        function postCreateStudent() {
            vm.loading = true;
            registerService.postCreateStudent(vm.ci, vm.name, vm.secondName, vm.registerCode, vm.email, vm.phoneNumber, function (result) {
                if (result === true) {
                   $state.go('login');
                } else {
                    vm.error = 'Create error';
                    vm.loading = false;
                }
            });
      };
  }

})();