;(function() {
  'use strict';

  angular.module("utepsa-notifications",['ui.router', 'ngStorage', 'infinite-scroll']);

  /**
   * Definition of the main app module and its dependencies
   */
  angular.module("utepsa-notifications").config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$stateProvider','$urlRouterProvider', '$httpProvider'];

  function config($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;

    $urlRouterProvider.otherwise('');

    // routes
    $stateProvider
        .state('main', {
            abstract: true,
            templateUrl:'app/core/pages/main.html',
            data : {
              cssClassnames : 'hold-transition skin-red sidebar-mini layout-boxed'
            }
        })
        .state('notifications', {
          url: '/',
          parent: 'main',
          templateUrl: 'app/notifications/notifications.html',
          controller: 'NotificationsController', 
            controllerAs: 'nc'
        })
        .state('studentProfile', {
            url:'/studentProfile/:ci',
            parent: 'main',
            controller: 'StudentProfileController',
            templateUrl:'app/studentProfile/studentProfile.html',
        })
        .state('login', {
            url:'/login',
            controller: 'LoginController',
            controllerAs: 'LoginController',
            templateUrl:'app/core/pages/login.html',
            data : {
              cssClassnames : 'hold-transition login-page'
            }
        })
        .state('register', {
            url:'/register',
            controller: 'RegisterController',
            controllerAs: 'RegisterController',
            templateUrl:'app/auth/register/register.html',
            data : {
              cssClassnames : 'hold-transition register-page'
            }
        })
        .state('historyNotes', {
            url:'/historyNotes',
            parent: 'main',
            controller: 'HistoryNotesController',
            templateUrl:'app/historyNotes/historyNotes.html',
        })
        .state('documentsStudent', {
            url:'/documents',
            parent: 'main',
            controller: 'DocumentsStudentController',
            templateUrl:'app/documentsStudent/documentsStudent.html',
        })

    ;
  }
  window.addEventListener("orientationchange", function(){
    screen.lockOrientation('portrait')
  });

  document.addEventListener('deviceready', onDeviceReady, false);

  document.addEventListener('resume', onResume, false);

  function onDeviceReady(){
    document.addEventListener("offline", onOffline, false);
    document.addEventListener("online", yourCallbackFunction, false);

    function onOffline (){
      $('#myModal').modal('show');
    }
    
    function yourCallbackFunction() {
      $('#myModal').modal('hide'); 
    }
  
  }

  function onResume(){

  }
  
  angular.module("utepsa-notifications").run(run);

    run.$inject = ['$rootScope', '$http','$location', '$state', '$localStorage'];

     function run($rootScope, $http, $location ,$state, $localStorage) {
        // keep user logged in after page refresh
            if (localStorage.getItem("currentUser") === null) {
              $state.transitionTo("login");
              event.preventDefault();
            }else {
              if ($localStorage.currentUser) {
                //$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
              }
            }

            // redirect to login page if not logged in and trying to access a restricted page
            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                var publicPages = ['/login', '/register'];

                var restrictedPage = publicPages.indexOf($location.path()) === -1;
                
                if (restrictedPage && !$localStorage.currentUser) {
                    $state.transitionTo("login");
                    event.preventDefault();
                }
            });
    }  
})();