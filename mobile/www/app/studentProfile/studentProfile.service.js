;(function() {

	angular.module("utepsa-notifications").factory("studentProfileService", studentProfileService);

	studentProfileService.$inject = ['$http','$stateParams','API','$localStorage'];

	function studentProfileService($http,$stateParams,API,$localStorage){

		function getStudentProfile(){
			if (!$localStorage.currentUser) {
				return false;
			}
			
			return $http({
			  method: 'GET',
			  url: API.url+'students/'+ $localStorage.currentUser.registerCode,
			  withCredentials: true,
	          headers: {
	          	'Content-Type': 'application/json; charset=utf-8'
	          }
			}).then(function(studentProfile) {
			  return studentProfile.data;
			})
			.catch(function(error) {
			  return error;
			});
		}

		var service = {
			getStudentProfile: getStudentProfile
		};

		return service;
	}

})();