;(function() {

  angular.module("utepsa-notifications").controller('StudentProfileController', StudentProfileController);

  StudentProfileController.$inject = ['$scope', 'resizeTemplate','studentProfileService', 'AuthenticationService'];

  function StudentProfileController($scope, resizeTemplate,studentProfileService, AuthenticationService) {
    var vm = this; 

    $scope.studentProfile = null;
    resizeTemplate.resize();
    getStudenProfile();
    $scope.spinnerStudentProfile = true;
    $scope.logout = logout;

    function logout(){
     AuthenticationService.Logout();
    }

    function getStudenProfile(){
      var promise = studentProfileService.getStudentProfile();
      if(promise){
        promise.then(function (result) {
          $scope.studentProfile = result;
          $scope.spinnerStudentProfile = false;
        });
      }
    }

  }

})();