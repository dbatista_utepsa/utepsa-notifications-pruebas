(function () {
    'use strict';
 
    angular
        .module('utepsa-notifications')
        .factory('AuthenticationService', AuthenticationService);
    
    AuthenticationService.$inject = ['$state', '$http', '$localStorage' ,'API'];

    function AuthenticationService($state, $http, $localStorage, API) {
        var service = {};
 
        service.Login = Login;
        service.Logout = Logout;
 
        return service;
 
        function Login(registerCode, password, callback) {
            $http({
              method: 'POST',
              url: API.url + 'auth',
              withCredentials: true,
              headers: { accountOrRegisterCode: registerCode, password: password }
            }).then(function(response) {
                // login successful if there's a token in the response
                if (response.status == 200) {
                    // store registerCode and token in local storage to keep user logged in between page refreshes
                    $localStorage.currentUser = { registerCode: registerCode, token: response.token };
                    // add jwt token to auth header for all requests made by the $http service
                    //$http.defaults.headers.common.Authorization = 'Bearer ' + response.token;

                    //execute callback with true to indicate successful login
                    callback(true);
                } else {
                    // execute callback with false to indicate failed login
                    callback(false);
                }
            })
            .catch(function(error) {
              callback(false);
            });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $state.transitionTo("login");
            event.preventDefault();
            //$http.defaults.headers.common.Authorization = '';
        }
    }
})();