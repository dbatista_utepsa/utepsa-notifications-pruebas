;(function() {

  	angular.module("utepsa-notifications").controller('LoginController', LoginController);

  	LoginController.$inject = ['$scope', '$state', 'resizeTemplate', 'AuthenticationService'];

  	function LoginController($scope, $state, resizeTemplate, AuthenticationService) {
    	resizeTemplate.resize();
    	var vm = this;

        vm.login = login;

        init();

        function init() {
            // reset login status
            //AuthenticationService.Logout();
        };

        // check is number
        var registerCodeIsNumber = function (accountOrRegisterCode) {
            var reg = new RegExp("^[-]?[0-9]+[\.]?[0-9]+$");
            return reg.test(accountOrRegisterCode);
        }

        var formatRegisterCode = function(accountOrRegisterCode, tamano) {

            if(registerCodeIsNumber(accountOrRegisterCode)) {
                return new Array(tamano + 1 - (accountOrRegisterCode + '').length).join('0') + accountOrRegisterCode;
            }else {
                return accountOrRegisterCode;
            }
        }

        function login() {
            vm.loading = true;
            var user =formatRegisterCode(vm.username,10);
            AuthenticationService.Login(user, vm.password, function (result) {
                if (result === true) {
                    $state.go('notifications');
                } else {
                    vm.error = 'Username or password is incorrect';
                    vm.loading = false;
                }
            });
    	};
  	}	

})();