;(function() {

	angular.module("utepsa-notifications").factory("registerService", registerService);

	registerService.$inject = ['$http','API'];

	function registerService($http,API){

		 function postCreateStudent(ci, name, secondName, registerCode, email, phoneNumber, callback) {

            $http({
              method: 'POST',
              url: API.url + 'students',
              withCredentials: true,
              data: { id:0,ci: ci, name: name, secondName: secondName, registerCode:registerCode, email:email, phoneNumber:phoneNumber}
            }).then(function(response) {
                if (response.status == 201) {
                    callback(true);
                } else {
                    callback(false);
                }
            })
            .catch(function(error) {
              callback(false);
            });
        }

		var service = {
			postCreateStudent: postCreateStudent
		};

		return service;
	}
})();