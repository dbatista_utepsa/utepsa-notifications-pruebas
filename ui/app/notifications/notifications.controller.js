;(function() {

  angular.module("utepsa-notifications").controller('NotificationsController', NotificationsController);

  NotificationsController.$inject = ['$scope', 'resizeTemplate' , 'notificationsService'];

  function NotificationsController($scope, resizeTemplate,notificationsService) {
    var vm = this;
    vm.notifications = null;
    vm.itemsNotifications = [];
    vm.busy = false;
    vm.rows = 0;

    vm.loadMore = function(){
      console.log(vm.busy);
      vm.busy = true;
      var promise = notificationsService.getNotifications(vm.rows);

      if(promise){
        promise.then(function (result) {
          vm.notifications = result;

          if(vm.notifications === undefined){
            return;
          }

          for(var i = 0; i < vm.notifications.length; i++){
            vm.itemsNotifications.push(vm.notifications[i]);

          }

          vm.rows = vm.rows + vm.notifications.length;
          if(vm.notifications.length < 6)
            vm.busy = true;
          else
            vm.busy = false;
        });
      }
    };

    resizeTemplate.resize();

    //getNotificatios();

    function getNotificatios(){
      $scope.busy = false;
      var promise = notificationsService.getNotifications($scope.rows);

      if(promise){
        promise.then(function (result) {
          $scope.notifications = result;
        });
      }
    }
  }

})();