'use strict';

angular.module('utepsa-notifications')
	.directive('headerTemplate',function(){
		return {
	        templateUrl:'app/core/directives/header/header.html',
	        restrict: 'A',
	        replace: false
    	}
	});

